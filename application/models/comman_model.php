<?php
class Comman_model extends CI_Model
{
	/*
 	* Admin by deafult role and access
	*/
	public $admin_role = array(
		'users','attendance','checkattendance','salesreport','attendancereport',
		'outlet','city','event','activity','target',
		'sales','notification','roles','usertype','product','stock'
	);
	public $admin_access = array('Type'=>'All','Level'=>'Edit');
	
	/*function getUserRole($userID)
	{
		$where = "`uar`.`UserID` = {$userID} AND `uar`.`Platform` != 'App'";
        $this->db->select('r.RoleCode');
        $this->db->from('userassignedroles as uar');
        $this->db->join('roles as r','r.RoleID = uar.RoleID');
        $this->db->where($where);
        $row = $this->db->get();
        $roleCodes = $row->result_array();
		return $roleCodes;
	}*/
	
	function getUserRoleTypeLevelPlatform($userID)
	{
		if($this->session->userdata("IsAdmin") != 1)
		{	
			$where = "`uar`.`UserID` = {$userID} AND `uar`.`Platform` != 'App'";
	        $this->db->select('r.RoleID, r.RoleCode, uar.Type, uar.Level, uar.Platform');
	        $this->db->from('userassignedroles as uar');
	        $this->db->join('roles as r','r.RoleID = uar.RoleID');
	        $this->db->where($where);
	        $row = $this->db->get();
	        $roleCodes = $row->result_array();
	    }else{
			$roleCodes = $this->admin_role;
		}
		return $roleCodes;
	}
	
	function getAllowUserTypeLevel($roleID,$userID)
	{
		if($this->session->userdata("IsAdmin") != 1)
		{
			$where = "`RoleID` = {$roleID} AND `UserID` = {$userID}";
	        $this->db->select('Type,Level');
	        $this->db->from('userassignedroles');
	        $this->db->where($where);
	        $row = $this->db->get();
	        $roleCodes = $row->result_array();
		}else{
			$roleCodes = $this->admin_access;
		}
		return $roleCodes;
	}
	
	function getCountries() {
		$query = $this->db->select('*')->from('countries')->get();
		return $query->result_array();
	}
	
	function getStatesAndTimeZone($country_id) {
		$this->db->select('states.stateID, states.stateName, countries.webCode as countrieCode, zone.zone_id, zone.zone_name');
		$this->db->from('states');
		$this->db->join('countries', 'countries.countryID = states.countryID');
		$this->db->join('zone', 'zone.country_code = countries.webCode');
		$this->db->where('states.countryID', $country_id);
		$query = $this->db->get();
		return $query->result_array();
	}
}
?>
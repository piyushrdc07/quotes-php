<?php
class Category_model extends CI_Model
{
	function insert($data)
	{
		$this->db->insert("tbl_categories", $data);
        $id = $this->db->insert_id();
		return $id;
	}
	
	function update($data)
	{
		try
        {
        	$this->db->update('tbl_categories', $data, array('id' => $data['id']));
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }		
	}
	
	function delete($data)
	{
		$this->db->delete("tbl_categories", $data);
        return true;
	}

	function get_all_categories()
	{
		$query = $this->db->select("*")->from('tbl_categories')->order_by("id","DESC")->get();
		return $query->result_array();
	}
	
	function get_all($data)
	{
		$where = "cate.status = 'y'";
		if(isset($data['ID']))
		{
			$where .= " AND cate.id = ".$data['ID'];
		}
		//$this->db->select('cate.*, (select count(id) from tbl_picture where find_in_set( `cate`.id,category_id )) as imageCount');
		//$this->db->from('tbl_categories as cate');
		//$this->db->join('tbl_picture as pic','cate.id = pic.category_id','left');
			$this->db->select('cate.* , COUNT(pic.id) imageCount from tbl_picture as pic
        INNER JOIN tbl_categories as cate
            ON find_in_set( cate.id,pic.category_id )');
	//	$this->db->from('tbl_picture as pic');
	//	$this->db->join('tbl_categories as cate',"find_in_set( 'cate.id','pic.category_id' )",'left');
		$this->db->where($where);
		$this->db->group_by('cate.category');
		$this->db->order_by('cate.category');
		$this->db->limit($data['Limit'],$data['Offset']);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		return $query->result_array();
	}

	function get_by_ids($ids)
	{
		$query = $this->db->select("id,category")->from('tbl_categories')->where_in('id',$ids)->order_by("id","DESC")->get();
		$result = $query->result_array();
		return $result;
	}
	
	function get_by_id($id)
	{
		$query = $this->db->select("*")->from('tbl_categories')->where(array("id" => $id))->order_by("id","DESC")->get();
		$result = $query->result_array();
		if($result)
		{
			return $result[0];
		}else{
			return $result;
		}
	}
	
	function get_all_ajax($sortBy = '', $sortOrder = '',$search = '',$limit = 10, $offset = 0,$findBy='')
	{
		$sortBy = (empty($sortBy) || $sortBy == "") ? "cate.category" : $sortBy;
        $sortOrder = (empty($sortOrder)) ? "asc" : $sortOrder;
		$where = "cate.status != ''";
		if(!empty($search))
		{
			$where .= " AND cate.category LIKE '%".$search."%'";
		}
		$this->db->select('cate.*');
		$this->db->from('tbl_categories as cate');
		$this->db->where($where);
		$this->db->order_by($sortBy,$sortOrder);
		if($limit > 0)
        {
			$this->db->limit($limit,$offset);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		return $query->result_array();
	}
	
}
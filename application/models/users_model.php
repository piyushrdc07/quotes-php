<?php
class Users_model extends CI_Model
{
	function insert($data)
	{
		$this->db->insert("tbl_users", $data);
        $id = $this->db->insert_id();
		return $id;
	}
	
	function update($data)
	{
		try
        {
            $this->db->update('tbl_users', $data, array('id' => $data['id']));
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }		
	}
	
	function checkForEmail($email){
		$query = $this->db->select("*")->from("tbl_users")->where(array('mail_id'=>$email))->get();
	    $records = $query->result_array();
		if(!empty($records)) {
			return $records[0];
		}
		return false;
	}

	function get_all()
	{
		$query = $this->db->select("*")->from('tbl_users')->order_by("id","DESC")->get();
		return $query->result_array();
	}
	
	function get_by_id($id) {
		$this -> db -> select('*');
		$this -> db -> from('tbl_users');
		$this -> db -> limit('1');
		$this -> db -> where("id", $id);
		$query = $this -> db -> get();
		$result = $query -> result_array();
		if (!empty($result)) {
			return $result['0'];
		}else{
			return false;
		}
	}
	
	/**
	 * Func tion For Get Last Id
	 * @return Id
	 */
	function IsValidSessionID($uniqueKey) {
		$this -> db -> select('id');
		$this -> db -> from('tbl_users');
		$this -> db -> limit('1');
		$this -> db -> where("SessionID", $uniqueKey);
		$query = $this -> db -> get();
		$result = $query -> result_array();

		if (empty($result)) {
			return true;
		}
		return false;
	}

	function GenerateNumber($length = 21) {
		// start with a blank password
		$password = "";
		// define possible characters - any character in this string can be
		// picked for use in the password, so if you want to put vowels back in
		// or add special characters such as exclamation marks, this is where
		// you should do it
		$possible = "12346789abcdefghijkmnpqrstuvwxyz";
		// we refer to the length of $possible a few times, so let's grab it now
		$maxlength = strlen($possible);
		// check for length overflow and truncate if necessary
		if ($length > $maxlength) {
			$length = $maxlength;
		}
		// set up a counter for how many characters are in the password so far
		$i = 0;
		// add random characters to $password until $length is reached
		while ($i < $length) {
			// pick a random character from the possible ones
			$char = substr($possible, mt_rand(0, $maxlength - 1), 1);
			// have we already used this character in $password?
			if (!strstr($password, $char)) {
				// no, so it's OK to add it onto the end of whatever we've already got...
				$password .= $char;
				// ... and increase the counter by one
				$i++;
			}
		}
		// done!
		return $password;
	}

	function getUserIDbySessionID($sessionID) {
		$this -> db -> select('id');
		$this -> db -> from('tbl_users');
		$this -> db -> limit('1');
		$this -> db -> where("SessionID", $sessionID);
		$query = $this -> db -> get();
		$result = $query -> result_array();
		if (!empty($result)) {
			return $result['0']['UserID'];
		}else{
			return false;
		}
	}
	
	function getUserIDbyFecabookID($facebookID) {
		$this -> db -> select('id');
		$this -> db -> from('tbl_users');
		$this -> db -> limit('1');
		$this -> db -> where("FacebookID", $facebookID);
		$query = $this -> db -> get();
		$result = $query -> result_array();
		if (!empty($result)) {
			return $result['0']['id'];
		}else{
			return false;
		}
	}
}
<?php $this->load->view('admin/header');?>
<script type="text/javascript">
	$(document).ready(function(){
        var PIXABAY_API_KEY = '1988758-568b6780351da41058ec14c6e';
	    $.each($(".left-side-menu-bar li"),function(key,value){
			if($(value).attr('pagename') == 'pixabay')
			{
				$(value).attr("class","active");
			}
		});
    });
</script>
<section class="vbox">
	<section class="scrollable padder">
		<div class="m-b-md">
			<!-- <h3 class="m-b-none">Manage User</h3> -->
		</div>
		<!-- success or Error Message Display -->
        <?php
        	$message = $this->message_stack->message('message');
			if($message != ""){
        ?>
        <div class="alert alert-<?php echo $this->message_stack->message('class'); ?>">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <i class="fa fa-ok-sign"></i><?php echo $message; ?>
        </div>
        <?php } ?>
        <div class="ajax-message hide">
        </div>
        <!-- End success or Error Message Display -->
		<div class="row"></div>
        <div class="filter form-inline pb20">
            <form id="filter_form" method="post" action="javascript:;" >
                <div class="form-group">
                    <label>Name</label>
                        <input type="text" name="image_id" class="form-control" id="image_id" value="">
                </div>
                <div class="form-group">
                    <button id="btnSearch" class="btn btn-success search" type="button">Search</button>
                    <button class="btn btn-danger show_all" onclick="window.location.href='<?php echo base_url(); ?>pixabay'" type="button">Show All</button>
                </div>
            </form>
        </div>
        <br />
		<section class="panel panel-blue">
		  <header class="panel-heading"> Pixabay List</header>
		  <div class="table-responsive">
		    <table id="dataTable" class="table table-striped b-t b-light">
                  <thead>
                    <tr>
                      <th class="th-sortable" data-toggle="class" width="50%" >Name</th>
                      <th class="th-sortable" data-toggle="class" width="30%" >Username</th>
                      <th class="edit-delet-action" width="20%" >Action</th>
                    </tr>
                  </thead>
                </table>
		  </div>
		  </section>
	</section>
</section>
<?php
	$this->assets->load("../js/datatables/datatables.css","admin");
	$this->assets->load("datatables/jquery.dataTables.min.js","admin");
    echo $this->assets->display_header_assets();
?>
<style>
	.dataTables_filter{
		position: absolute;
    	right: 0;
    	top: 0;
	}
</style>
<script type="text/javascript">
    var ajax_table = [];
    $(document).ready(function(){
        $('#btnSearch').click(function(){
            ajax_table.fnDraw();
        });
        getData();
    });
    function getData(){
        ajax_table = $("#dataTable").dataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "sAjaxSource": "view",
            "fnServerData": function(sSource, aoData, fnCallback, oSettings) {
                aoData.push({'name':'search','value':$('#image_id').val()});
                //aoData.push({'name':'ServiceProviderID','value':$('#ServiceProviderID').val()},{'name':'StartDate','value':$("#StartDate").val()});
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": site_url + "pixabay/view",
                    "data": aoData,
                    "headers" : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "success": fnCallback
                });
            },
            "retrieve": true,
            "paging": true,
            "pageLength": 10,
            "ColumnDefs": [
                {"width": "50%", "name": "Image Path", "targets": 0, "orderable": false},
                {"width": "30%", "name": "Username", "targets": 1, "orderable": false},
                {"width": "20%", "name": "Action", "targets": 2, "orderable": false}
            ],
            "aoColumns": [
			    null,
			    null,
			    { "sClass": "edit-delet-action" }
			],
            "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
            oLanguage: {
                sProcessing: '<div class="overlay-fullpage" ><div class="overlay-loader">Loading Data...<br><img src="' + site_url + 'application/assets/admin/images/ajax-loader.gif"></div></div>'
            },
            "fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                $("#row_count").text(iTotal);
                if(iTotal == 0)
                {
                    iStart = 0;
                }
                return 'Showing '+iStart+' to '+iEnd+' of '+iTotal+' entries';
            }

        });
    }
    function getData_res(response){
        if(response != null){
            ajax_table.dataTable().fnDraw();
        }
    }
</script>
<?php $this->load->view('admin/footer');?>
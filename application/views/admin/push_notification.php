<?php $this->load->view("admin/header"); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$.each($(".left-side-menu-bar li"),function(key,value){
			if($(value).attr('pagename') == 'notification')
			{
				$(value).attr("class","active");
			}
		});
		$('#ImagePhoto').change(function(event){
			var tmppath = URL.createObjectURL(event.target.files[0]);
			$("#ImagePhotoPreview").fadeIn("fast").attr('src',tmppath);
		});
		
		$('#Image').change(function(event){
			var tmppath = URL.createObjectURL(event.target.files[0]);
			$("#ImagePreview").fadeIn("fast").attr('src',tmppath);
		});
	});
</script>
<section class="vbox" id="bjax-el">
	<section class="scrollable wrapper-lg">
	<?php

        	$message = $this->message_stack->message('message');

			if($message != ""){

        ?>

        <div class="alert alert-<?php echo $this->message_stack->message('class'); ?>">

            <button data-dismiss="alert" class="close" type="button">×</button>

            <i class="fa fa-ok-sign"></i><?php echo $message; ?>

        </div>
        <div class="row"></div>
        <?php } ?>
		<div class="row">
			<section class="panel panel-default col-md-12">
				<header class="panel-heading font-bold">Push Notification</header>
				<div class="panel-body">
					<form name="notification_form" id="notification_form" class="form-horizontal" data-validate="parsley" action="<?php echo base_url(); ?>notification/add_post" method="post" enctype="multipart/form-data">
						
						<div class="line line-dashed b-b line-lg pull-in"></div>
						
						<?php $required = 'data-required="true"'; ?>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">Photo</label>
							<div class="col-sm-4">
								<input name="Image" id="Image" type="file" class="filestyle" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline v-middle input-s">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label"></label>
							<div class="col-sm-4">
							<?php
								$deafultImage = $this->assets->url('photo.jpg','admin');
							?>
								<img id="ImagePreview" width="100" class="preview" src="<?php echo $deafultImage; ?>" onerror="this.src='<?=$this->assets->url('photo.jpg','admin');?>'">
							</div>
						</div>

						<div class="line line-dashed b-b line-lg pull-in"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="image_name">Message</label>
							<div class="col-sm-4">
								<input type="text" name="message" class="form-control" id="message" value="" data-required="true">
							</div>
						</div>
						<div class="line line-dashed b-b line-lg pull-in"></div>

						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-id-1">Title</label>
							<div class="col-sm-4">
								<input type="textbox" class="form-control" name="title" value="" data-required="true">
							</div>
						</div>
						<div class="line line-dashed b-b line-lg pull-in"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-id-1">Is Sound?</label>
							<div class="col-sm-4">
							<input type="radio" name="sound" value="1" checked="" >Yes<br>
							<input type="radio" name="sound" value="0" >No<br>
							</div>
						</div>
						<div class="line line-dashed b-b line-lg pull-in"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-id-1">Is Vibrate?</label>
							<div class="col-sm-4">
							<input type="radio" name="vibrate" value="1" checked="" >Yes<br>
							<input type="radio" name="vibrate" value="0" >No<br>
							</div>
						</div>
						<div class="line line-dashed b-b line-lg pull-in"></div>
						<div class="form-group">
							<div class="col-sm-4 col-sm-offset-2">
								<button type="submit" class="btn btn-info">Send Notification</button>
							</div>
						</div>
					</form>
				</div>
			</section>
		</div>
	</section>
</section>
<?php
	$this->assets->load("file-input/bootstrap-filestyle.min.js",'admin');
	echo $this->assets->display_header_assets();
?>
<?php $this->load->view("admin/footer"); ?>
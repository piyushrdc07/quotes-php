<?php $this->load->view('admin/header');?>
<script type="text/javascript">
	$(document).ready(function(){
		$.each($(".dropdown-menu li"),function(key,value){
			if($(value).attr('pagename') == 'setting')
			{
				$(value).attr("class","active");
			}
		});
		$('.filestyle').change(function(event){
			var tmppath = URL.createObjectURL(event.target.files[0]);
			$(".preview").fadeIn("fast").attr('src',tmppath);
		});
	});
</script>
<section class="vbox" id="bjax-el">
	<section class="scrollable wrapper-lg">
		<!-- success or Error Message Display -->
		<?php
			$message = $this->message_stack->message('message');
			if($message != ""):
		?>
		<div class="alert alert-<?php echo $this->message_stack->message('class'); ?>">
			<button data-dismiss="alert" class="close" type="button">×</button>
			<i class="fa fa-ok-sign"></i><?php echo $message;
			?>
		</div>
		<?php endif;
		?>
		<!-- End success or Error Message Display -->
		<div class="row">
			<section class="panel panel-default col-md-12">
				<header class="panel-heading font-bold">Update Setting</header>
				<div class="panel-body">
					<div class="tab-content">
						<div id="home" class="tab-pane active">
							<div class="panel-body">
								<div class="pull-right"><b>Last Login Date : <?php if(isset($admin[0]['AdminID'])){echo date("d/m/Y h:i a",strtotime($admin[0]['LastLoginAt']));} ?></b></div>
								<form data-validate="parsley" class="form-horizontal" method="post" action="<?php echo base_url().'admin/setting' ?>" enctype="multipart/form-data">
									<input type="hidden" name="AdminID" value="<?php if(isset($admin[0]['AdminID'])){echo $admin[0]['AdminID'];} ?>" />
									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-id-1">Download Link</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="Download" data-type="url" value="<?php if(isset($admin[0]['Download'])){echo $admin[0]['Download'];} ?>">
										</div>
									</div>
									<div class="line line-dashed b-b line-lg pull-in"></div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-id-1">Information Link</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="input-id-1" name="Information" data-type="url" value="<?php if(isset($admin[0]['Information'])){echo $admin[0]['Information'];} ?>">
										</div>
									</div>
									<div class="line line-dashed b-b line-lg pull-in"></div>
									<div class="form-group">
										<div class="col-sm-4 col-sm-offset-2">

											<button type="submit" class="btn btn-info">
												Update
											</button>
											<button type="button" class="btn btn-danger" onclick="window.history.back();">
												Cancel
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>

		</div>
	</section>
</section>
<?php $this->load->view('admin/footer');?>

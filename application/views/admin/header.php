<!DOCTYPE html>
<html lang="en" class="app">
<head>
  <meta charset="utf-8" />
  <title>Quotes | Admin Panel</title>
  <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <?php
    //$this->assets->load("js/jPlayer/jplayer.flat.css",'admin');
	$this->assets->load("bootstrap.css",'admin');
	$this->assets->load("animate.css",'admin');
	//for local
	$this->assets->load("font-awesome.min.css",'admin');
	$this->assets->load("simple-line-icons.css",'admin');

	//End for local
	$this->assets->load("font.css",'admin');
	$this->assets->load("app.css",'admin');
	//load javascript file
	$this->assets->load("jquery.min.js",'admin');
	$this->assets->load("common.js",'admin');
	$this->assets->load("bootstrap.js",'admin');
	$this->assets->load("app.js",'admin');
	$this->assets->load("slimscroll/jquery.slimscroll.min.js",'admin');
	$this->assets->load("app.plugin.js",'admin');
	$this->assets->load("jquery.ui.touch-punch.min.js",'admin');
	$this->assets->load("jquery-ui-1.10.3.custom.min.js",'admin');
	$this->assets->load("parsley/parsley.min.js",'admin');
	$this->assets->load("parsley/parsley.extend.js",'admin');
	$this->assets->load("ckeditor/ckeditor.js","admin");

	$this->assets->load("ui/jquery-ui.css","admin");
	$this->assets->load("ui/jquery-ui.js","admin");
	$this->assets->load("ui/jquery-ui-timepicker-addon.js","admin");

    echo $this->assets->display_header_assets();

?>
    <!--[if lt IE 9]>
    <script src="<?php echo $this->assets->url('ie/html5shiv.js','admin'); ?>"></script>
    <script src="<?php echo $this->assets->url('ie/respond.min.js','admin'); ?>"></script>
    <script src="<?php echo $this->assets->url('ie/excanvas.js','admin'); ?>js/"></script>
  <![endif]-->
</head>
<style type="text/css">
  .overlay-fullpage {
      background: rgba(0, 0, 0, 0.4) none repeat scroll 0 0;
      display: block;
      height: 100%;
      left: 0;
      min-height: 100%;
      position: fixed;
      top: 0;
      width: 100%;
      z-index: 10000;
  }
  .current{
    color: #36B0C8;
    border: 1px solid #36B0C8 !important;
  }
</style>
<body class="">
  <div class="overlay-fullpage hide">
    <div class="overlay-loader"><img src="<?php echo $this->assets->url("ajax-loader.gif","admin"); ?>" /></div>
  </div>
  <div class="overlay hide">
  	<div class="overlay-loader"><img src="<?php echo $this->assets->url("ajax-loader.gif","admin"); ?>" /></div>
  </div>
  <section class="vbox">
    <header class="bg-white-only header header-md navbar navbar-fixed-top-xs">
      <div class="navbar-header aside bg-info dk">
        <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen,open" data-target="#nav,html">
          <i class="icon-list"></i>
        </a>
        <a href="<?php echo base_url(); ?>" class="navbar-brand text-lt">
          <i class="icon-diamond"></i>
          <img src="<?php echo $this->assets->url('logo.png'); ?>" alt="." class="hide">
          <span class="hidden-nav-xs m-l-sm">Quotes</span>
        </a>
        <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user">
          <i class="icon-settings"></i>
        </a>
      </div>      <ul class="nav navbar-nav hidden-xs">
        <li>
          <a href="#nav,.navbar-header" data-toggle="class:nav-xs,nav-xs" class="text-muted">
            <i class="fa fa-indent text"></i>
            <i class="fa fa-dedent text-active"></i>
          </a>
        </li>
      </ul>
      <div class="navbar-right ">
        <ul class="nav navbar-nav m-n hidden-xs nav-user user">
          <li class="dropdown">
            <a href="javascript:;" class="dropdown-toggle bg clear" data-toggle="dropdown">
               <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
               	<?php
					$this->load->model('admin_model', 'admin');
                	$admin = $this->admin->get_data_by_id($this->session->userdata('AdminID'));
					if(isset($admin[0]['Photo']) && $admin[0]['Photo'] != ""){
						$img = $admin[0]['Photo'];
					}
                 ?>
                <img src="<?php echo base_url().APPPATH.'upload/admin/thumb_small/'.$img; ?>" onerror="this.src='<?=$this->assets->url('photo.jpg','admin');?>'" >
              </span>
              <?php echo $this->session->userdata("Name"); ?><b class="caret"></b>
            </a>
            <ul class="dropdown-menu animated fadeInRight">
              <li pagename="profile">
                <a href="<?=base_url().'admin/profile'?>">Profile</a>
              </li>
              <li class="divider"></li>
              <!-- <li pagename="setting">
                <a href="<?=base_url().'admin/setting'?>">Setting</a>
              </li>
              <li class="divider"></li> -->
              <li>
                <a href="<?=base_url().'admin/sign_out'?>" data-toggle="ajaxModal1" >Logout</a>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </header>
    <section>
      <section class="hbox stretch">
        <!-- .aside -->
        <aside class="bg-black dk aside hidden-print" id="nav">
          <section class="vbox">
            <section class="w-f-md scrollable">
              <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-railOpacity="0.2">
                <!-- nav -->
                <nav class="nav-primary hidden-xs">
                  <ul class="nav left-side-menu-bar" data-ride="collapse">
                    <li pagename="category">
                      <a href="<?php echo base_url(); ?>category">
                        <i class="icon-wallet text-info"></i>
                        <span class="font-bold">Category</span>
                      </a>
                    </li>
					         <!-- <li pagename="pictures">
                      <a href="<?php echo base_url(); ?>pictures">
                        <i class="icon-wallet text-info"></i>
                        <span class="font-bold">Pictures</span>
                      </a>
                    </li>
                    <li pagename="flickr">
                      <a href="<?php echo base_url(); ?>flickr">
                          <i class="icon-wallet text-info"></i>
                          <span class="font-bold">Flickr</span>
                      </a>
                    </li>
                    <li pagename="unsplash">
                      <a href="<?php echo base_url(); ?>unsplash">
                          <i class="icon-wallet text-info"></i>
                          <span class="font-bold">Unsplash</span>
                      </a>
                    </li>
                    <li pagename="pixabay">
                      <a href="<?php echo base_url(); ?>pixabay">
                          <i class="icon-wallet text-info"></i>
                          <span class="font-bold">Pixabay</span>
                      </a>
                    </li>
					         <li pagename="tags">
                      <a href="<?php echo base_url(); ?>tags">
                        <i class="icon-wallet text-info"></i>
                        <span class="font-bold">Tags</span>
                      </a>
                    </li>
					         <li pagename="copyrights">
                      <a href="<?php echo base_url(); ?>copyrights">
                        <i class="icon-wallet text-info"></i>
                        <span class="font-bold">Copyrights</span>
                      </a>
                    </li>
                    <li pagename="notification">
                      <a href="<?php echo base_url(); ?>notification">
                        <i class="icon-wallet text-info"></i>
                        <span class="font-bold">Push Notification</span>
                      </a>
                    </li> -->
                    <li class="m-b hidden-nav-xs"></li>
                  </ul>
                </nav>
                <!-- / nav -->
              </div>
            </section>
          </section>
        </aside>
        <!-- /.aside -->
        <section id="content">
        	<!-- Header -->
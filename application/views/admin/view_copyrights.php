<?php $this->load->view('admin/header');?>
<script type="text/javascript">
	$(document).ready(function(){
	    $.each($(".left-side-menu-bar li"),function(key,value){
			if($(value).attr('pagename') == 'copyrights')
			{
				$(value).attr("class","active");
			}
		});
    });
</script>
<section class="vbox">
	<section class="scrollable padder">
		<div class="m-b-md">
			<!-- <h3 class="m-b-none">Manage User</h3> -->
		</div>
		<!-- success or Error Message Display -->
        <?php
        	$message = $this->message_stack->message('message');
			if($message != ""){
        ?>
        <div class="alert alert-<?php echo $this->message_stack->message('class'); ?>">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <i class="fa fa-ok-sign"></i><?php echo $message; ?>
        </div>
        <?php } ?>
        <div class="ajax-message hide">
        </div>
        <!-- End success or Error Message Display -->
		<div class="row"></div>
		<section class="panel panel-blue">
		  <header class="panel-heading"> Copyrights List<a class="pull-right" href="<?php echo base_url()."copyrights/add" ?>"><i style="color:#FFF;" class="fa fa-plus fa-1x">&nbsp;Add copyrights</i></a> </header>
		  <div class="table-responsive">
		    <table class="table table-striped b-t b-light">
                  <thead>
                    <tr>
                      <th class="th-sortable" data-toggle="class" width="90%" >Name</th>
                      <th class="edit-delet-action" width="10%" >Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php foreach($copyrights as $copyrights): ?>
                  		<tr>
				          <td><?php echo $copyrights["copyright_title"]; ?></td>
				          <td class="edit-delet-action">
	                      	<a href="<?php echo base_url()."copyrights/add?id={$copyrights['id']}" ?>"><i class="fa fa-pencil"></i></a>
	                      	<a onClick="return confirm('Are You Sure Delete Record!');" href="<?php echo base_url()."copyrights/delete?id={$copyrights['id']}" ?>"><i class="fa fa-times text-danger"></i></a>
	                      </td>
	                    </tr>	
                  	<?php endforeach; ?>
                  </tbody>
                </table>
		  </div>
		  </section>
	</section>
</section>
<?php $this->load->view('admin/footer');?>
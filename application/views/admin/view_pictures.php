<?php $this->load->view('admin/header');?>
<script type="text/javascript">
	$(document).ready(function(){
	    $.each($(".left-side-menu-bar li"),function(key,value){
			if($(value).attr('pagename') == 'pictures')
			{
				$(value).attr("class","active");
			}
		});
    });
</script>
<section class="vbox">
	<section class="scrollable padder">
		<div class="m-b-md">
			<!-- <h3 class="m-b-none">Manage User</h3> -->
		</div>
		<!-- success or Error Message Display -->
        <?php
        	$message = $this->message_stack->message('message');
			if($message != ""){
        ?>
        <div class="alert alert-<?php echo $this->message_stack->message('class'); ?>">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <i class="fa fa-ok-sign"></i><?php echo $message; ?>
        </div>
        <?php } ?>
        <div class="ajax-message hide">
        </div>
        <!-- End success or Error Message Display -->
		<div class="row"></div>
		<div class="filter form-inline pb20">
        	<form id="filter_form" method="post" action="javascript:;" >
				<div class="form-group">
					<label>Category</label>
					<select class="form-control" id="category_id" name="category_id">
						<option value="">All</option>
						<?php foreach($categories as $category): ?>
							<option value="<?php echo $category['id']; ?>"><?php echo $category['category']; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group">
					<button id="btnSearch" class="btn btn-success search" type="button">Search</button>
					<button class="btn btn-danger show_all" onclick="window.location.href='<?php echo base_url(); ?>pictures'" type="button">Show All</button>
				</div>
			</form>
		</div>
		<section class="panel panel-blue">
		  <header class="panel-heading"> pictures List<a class="pull-right" href="<?php echo base_url()."pictures/add" ?>"><i style="color:#FFF;" class="fa fa-plus fa-1x">&nbsp;Add pictures</i></a> </header>
		  <div class="table-responsive">
		    <table id="dataTable" class="table table-striped b-t b-light">
                  <thead>
                    <tr>
                    	
                      <th class="th-sortable" data-toggle="class" width="20%" >Title</th>
                      <th class="th-sortable" data-toggle="class" width="20%" >Picture</th>
					  <th class="th-sortable" data-toggle="class" width="20%" >Description</th>
					  <th class="th-sortable" data-toggle="class" width="20%" >Location</th>
					  <th class="th-sortable" data-toggle="class" width="10%" >Photographer</th>
                      <th class="edit-delet-action" width="10%" >Action</th>
                    </tr>
                  </thead>
                </table>
		  </div>
		  </section>
	</section>
</section>
<?php
	$this->assets->load("../js/datatables/datatables.css","admin");
	$this->assets->load("datatables/jquery.dataTables.min.js","admin");
    echo $this->assets->display_header_assets();
?>
<style>
	.dataTables_filter{
		position: absolute;
    	right: 0;
    	top: 0;
	}
	.pb20{
		padding-bottom: 20px;
	}
</style>
<script type="text/javascript">
    var ajax_table = [];
    $(document).ready(function(){
        $('#btnSearch').click(function(){
            ajax_table.fnDraw();
        });
        getData();
    });
    function getData(){
        ajax_table = $("#dataTable").dataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": true,
            "sAjaxSource": "view",
            "fnServerData": function(sSource, aoData, fnCallback, oSettings) {
                aoData.push({'name':'category_id','value':$('#category_id').val()});
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": site_url + "pictures/view",
                    "data": aoData,
                    "headers" : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "success": fnCallback
                });
            },
            "retrieve": true,
            "paging": true,
            "pageLength": 10,
            "ColumnDefs": [
                {"width": "20%", "name": "Title", "targets": 0},
                {"width": "20%", "name": "Picture", "targets": 1,"orderable": false},
                {"width": "20%", "name": "Description", "targets": 2, "orderable": false},
                {"width": "20%", "name": "Location", "targets": 3},
                {"width": "10%", "name": "Photographer", "targets": 4},
                {"width": "10%", "name": "Action", "targets": 5, "orderable": false}
            ],
            "aoColumns": [
			    null,
			    null,
			    null,
			    null,
			    null,
			    { "sClass": "edit-delet-action" }
			],
            "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
            oLanguage: {
                sProcessing: 'Loading Data...<br><img src="' + site_url + 'application/assets/admin/images/ajax-loader.gif">'
            },
            "fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                $("#row_count").text(iTotal);
                if(iTotal == 0)
                {
                    iStart = 0;
                }
                return 'Showing '+iStart+' to '+iEnd+' of '+iTotal+' entries';
            }
        });
    }
    function getData_res(response){
        if(response != null){
            ajax_table.dataTable().fnDraw();
        }
    }
</script>
<?php $this->load->view('admin/footer');?>
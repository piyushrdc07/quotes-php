<?php $this->load->view('admin/header');?>

<script type="text/javascript">

$(document).ready(function(){

	$.each($(".dropdown-menu li"),function(key,value){

		if($(value).attr('pagename') == 'profile')

		{

			$(value).attr("class","active");

		}

	});

	$('.filestyle').change(function(event){

		var tmppath = URL.createObjectURL(event.target.files[0]);

		$(".preview").fadeIn("fast").attr('src',tmppath);

	});

});

</script>

<section class="vbox" id="bjax-el">

	<section class="scrollable wrapper-lg">

		<!-- success or Error Message Display -->

		<?php

			$message = $this->message_stack->message('message');

			if($message != ""):

		?>

		<div class="alert alert-<?php echo $this->message_stack->message('class'); ?>">

			<button data-dismiss="alert" class="close" type="button">

				×

			</button>

			<i class="fa fa-ok-sign"></i><?php echo $message;

			?>

		</div>

		<?php endif;

		?>

		<!-- End success or Error Message Display -->

		<div class="row">

			<section class="panel panel-default">

				<header class="panel-heading bg-light">

					<ul class="nav nav-tabs nav-justified">

						<li class="active">

							<a data-toggle="tab" href="#home">Update Profile</a>

						</li>

						<li class="">

							<a data-toggle="tab" href="#profile">Change Password</a>

						</li>

					</ul>

				</header>

				<div class="panel-body">

					<div class="tab-content">

						<div id="home" class="tab-pane active">

							<div class="panel-body">

								<!-- Flash alert started -->

								<?php

									$message = $this->session->flashdata('message');

									$msgClass =  $this->session->flashdata('class');

									if(!empty($message)){

									echo "<div class='alert alert-".$msgClass."' role='alert'>".$message."</div>";

									echo "<br /><br />";

									}

								?>

								<!-- Flash alert ended -->

								<div class="pull-right"><b>Last Login Date : <?php if(isset($admin[0]['AdminID'])){echo date("d/m/Y h:i a",strtotime($admin[0]['LastLoginAt']));} ?></b></div>

								<form class="form-horizontal" method="post" action="<?php echo base_url().'admin/profile' ?>" enctype="multipart/form-data">

									<input type="hidden" name="AdminID" value="<?php if(isset($admin[0]['AdminID'])){echo $admin[0]['AdminID'];} ?>" />

									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-id-1">Username</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="input-id-1" name="Username" value="<?php if(isset($admin[0]['Username'])){echo $admin[0]['Username'];} ?>">
										</div>
									</div>
									<div class="line line-dashed b-b line-lg pull-in"></div>

									<div class="form-group">

										<label class="col-sm-2 control-label" for="input-id-1">Name</label>

										<div class="col-sm-4">

											<input type="text" class="form-control" id="input-id-1" name="Name" value="<?php if(isset($admin[0]['Name'])){echo $admin[0]['Name'];} ?>">

										</div>

									</div>



									<div class="line line-dashed b-b line-lg pull-in"></div>

									<div class="form-group">

										<label class="col-sm-2 control-label" for="input-id-1">Email</label>

										<div class="col-sm-4">

											<input type="text" class="form-control" id="input-id-1" name="Email" value="<?php if(isset($admin[0]['Email'])){echo $admin[0]['Email'];} ?>">

										</div>

									</div>

									

									<div class="line line-dashed b-b line-lg pull-in"></div>

									<div class="form-group">
										<label class="col-sm-2 control-label" for="PerPageRecord">Per Page Record</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="PerPageRecord" name="PerPageRecord" value="<?php if(isset($admin[0]['PerPageRecord'])){echo $admin[0]['PerPageRecord'];} ?>">
										</div>
									</div>
									<div class="line line-dashed b-b line-lg pull-in"></div>

									<div class="form-group">

										<div class="col-sm-4 col-sm-offset-2">



											<button type="submit" class="btn btn-info">

												Update

											</button>

											<button type="button" class="btn btn-danger" onclick="window.history.back();">

												Cancel

											</button>

										</div>

									</div>

								</form>

							</div>

						</div>

						<div id="profile" class="tab-pane">

							<div class="panel-body">

								<div class="pull-right"><b>Last Login Date : <?php if(isset($admin[0]['AdminID'])){echo date("d/m/Y h:i a",strtotime($admin[0]['LastLoginAt']));} ?></b></div>

								<form data-validate="parsley" class="form-horizontal" method="post" action="<?php echo base_url().'admin/profile' ?>" id="change-pwd">

									<div class="form-group">

									<label class="col-sm-2 control-label">Old Password</label>

									<div class="col-sm-4">

									<input type="password" name="oldPassword" class="form-control" data-required="true">

									</div>

									</div>

									<div class="line line-dashed b-b line-lg pull-in"></div>

									<div class="form-group">

										<label class="col-sm-2 control-label">New Password</label>

										<div class="col-sm-4">

											<input type="password" name="Password" id="Password" class="form-control">

										</div>

									</div>

									<div class="line line-dashed b-b line-lg pull-in"></div>

									<div class="form-group">

										<label class="col-sm-2 control-label">Confirm Password</label>

										<div class="col-sm-4">

											<input type="password" class="form-control" data-equalto="#Password" data-equalto-message="New passwords do not match">

										</div>

									</div>

									<div class="line line-dashed b-b line-lg pull-in"></div>



									<div class="form-group">

										<div class="col-sm-4 col-sm-offset-2">

											<button type="submit" class="btn btn-info" id="pwd-change-btn">

												Update

											</button>

											<button type="button" class="btn btn-danger" onclick="window.history.back();">

												Cancel

											</button>

										</div>

									</div>

								</form>

							</div>



						</div>

					</div>

				</div>

			</section>



		</div>

	</section>

</section>

<?php $this->load->view('admin/footer');?>


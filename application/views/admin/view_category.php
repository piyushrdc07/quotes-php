<?php $this->load->view('admin/header');?>
<script type="text/javascript">
	$(document).ready(function(){
	    $.each($(".left-side-menu-bar li"),function(key,value){
			if($(value).attr('pagename') == 'category')
			{
				$(value).attr("class","active");
			}
		});
    });
</script>
<section class="vbox">
	<section class="scrollable padder">
		<div class="m-b-md">
			<!-- <h3 class="m-b-none">Manage User</h3> -->
		</div>
		<!-- success or Error Message Display -->
        <?php
        	$message = $this->message_stack->message('message');
			if($message != ""){
        ?>
        <div class="alert alert-<?php echo $this->message_stack->message('class'); ?>">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <i class="fa fa-ok-sign"></i><?php echo $message; ?>
        </div>
        <?php } ?>
        <div class="ajax-message hide">
        </div>
        <!-- End success or Error Message Display -->
		<div class="row"></div>
		<section class="panel panel-blue">
		  <header class="panel-heading"> Category List<a class="pull-right" href="<?php echo base_url()."category/add" ?>"><i style="color:#FFF;" class="fa fa-plus fa-1x">&nbsp;Add Category</i></a> </header>
		  <div class="table-responsive">
		    <table id="dataTable" class="table table-striped b-t b-light">
                  <thead>
                    <tr>
                      <th class="th-sortable" data-toggle="class" width="80%" >Name</th>
                      <th class="th-sortable" data-toggle="class" width="10%" >Status</th>
                      <th class="edit-delet-action" width="10%" >Action</th>
                    </tr>
                  </thead>
                </table>
		  </div>
		  </section>
	</section>
</section>
<?php
	$this->assets->load("../js/datatables/datatables.css","admin");
	$this->assets->load("datatables/jquery.dataTables.min.js","admin");
    echo $this->assets->display_header_assets();
?>
<style>
	.dataTables_filter{
		position: absolute;
    	right: 0;
    	top: 0;
	}
</style>
<script type="text/javascript">
    var ajax_table = [];
    $(document).ready(function(){
        $('#btnSearch').click(function(){
            ajax_table.fnDraw();
        });
        getData();
    });
    function getData(){
        ajax_table = $("#dataTable").dataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": true,
            "sAjaxSource": "view",
            "fnServerData": function(sSource, aoData, fnCallback, oSettings) {
                //aoData.push({'name':'AdvertiserID','value':$('#AdvertiserID').val()},{'name':'EndDate','value':$("#EndDate").val()});
                //aoData.push({'name':'ServiceProviderID','value':$('#ServiceProviderID').val()},{'name':'StartDate','value':$("#StartDate").val()});
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": site_url + "category/view",
                    "data": aoData,
                    "headers" : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "success": fnCallback
                });
            },
            "retrieve": true,
            "paging": true,
            "pageLength": 10,
            "ColumnDefs": [
                {"width": "80%", "name": "Category", "targets": 0},
                {"width": "10%", "name": "Status", "targets": 1},
                {"width": "10%", "name": "Action", "targets": 2, "orderable": false}
            ],
            "aoColumns": [
			    null,
			    null,
			    { "sClass": "edit-delet-action" }
			],
            "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
            oLanguage: {
                sProcessing: 'Loading Data...<br><img src="' + site_url + 'application/assets/admin/images/ajax-loader.gif">'
            },
            "fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                $("#row_count").text(iTotal);
                if(iTotal == 0)
                {
                    iStart = 0;
                }
                return 'Showing '+iStart+' to '+iEnd+' of '+iTotal+' entries';
            }

        });
    }
    function getData_res(response){
        if(response != null){
            ajax_table.dataTable().fnDraw();
        }
    }
</script>
<?php $this->load->view('admin/footer');?>
<?php $this->load->view("admin/header"); ?>
    <script type="text/javascript">
        var PIXABAY_API_KEY = '1988758-568b6780351da41058ec14c6e';
        var PIXABAY_API_URL = "https://pixabay.com/api/";
        $(document).ready(function(){
            $.each($(".left-side-menu-bar li"),function(key,value){
                if($(value).attr('pagename') == 'pixabay')
                {
                    $(value).attr("class","active");
                }
            });
            $('.filestyle').change(function(event){
                var tmppath = URL.createObjectURL(event.target.files[0]);
                $(".preview").fadeIn("fast").attr('src',tmppath);
            });
            $('#categoty_form').submit(function(){
                $(this).parsley().validate();
                if ($(this).parsley().isValid()) {
                    show_full_page_loader();
                }
            })
            $("#get_pixabay_image").click(function(){
                show_full_page_loader();
                var _img_id = $("#image_id").val();
                if(_img_id == 0)
                {
                    hide_full_page_loader();
                    alert("Please enter valid image id");
                    return false;
                }
                var postData = {
                    key:PIXABAY_API_KEY,
                    id:_img_id
                };
                var type = 'GET';
                var dataType = 'JSON';
                AjaxCall(PIXABAY_API_URL,type,postData,dataType,'fetch_pixabay_image');
            });
        });

        function fetch_pixabay_image(data)
        {
            if (data.totalHits > 0)
            {
                var _img_hash_id = $("#image_hash_id").val();
                var postData = {
                    key:PIXABAY_API_KEY,
                    id:_img_hash_id,
                    response_group : 'high_resolution',
                    pretty:true
                };
                var type = 'GET';
                var dataType = 'JSON';
                AjaxCall(PIXABAY_API_URL,type,postData,dataType,'image_response',data.hits[0]);
            }
            else
            {
                hide_full_page_loader();
                alert("Image not found, please try another image.");
            }
        }

        function image_response(data,image)
        {
            if (data.totalHits > 0)
            {
                if(data.hits[0].fullHDURL != undefined && data.hits[0].fullHDURL != '')
                {
                    image.fullHDURL = data.hits[0].fullHDURL;
                    image.userProfile = "https://pixabay.com/users/"+data.hits[0].user+"-"+data.hits[0].user_id+"/";
                    image.userImage = data.hits[0].imageURL;
                    //console.log(image);
                    set_data_to_form(image);
                }else{
                    hide_full_page_loader();
                    alert("This image size to small, please try another image.");
                }
            }
            else
            {
                hide_full_page_loader();
                console.log("Image size not found.");
            }
        }

        function set_data_to_form(data)
        {
            $("#image_name").val('');
            $("#image_path").val(data.fullHDURL);
            $("#ImagePreview").attr('src',data.previewURL);
            $("#description").text(data.userProfile);
            $("#photographer_name").val(data.user);
            $("#photographer_photo").val(data.userImageURL);
            $("#ImagePhotoPreview").attr('src',data.userImageURL);
            $("#photo_source_link").val(data.pageURL);
            $("#copyright_id").val(10);
            $("#tags").val(data.tags);
            hide_full_page_loader();
        }
    </script>

    <section class="vbox" id="bjax-el">
        <section class="scrollable wrapper-lg">
            <!-- success or Error Message Display -->
            <?php
            $message = $this->message_stack->message('message');
            if($message != ""){
                ?>
                <div class="alert alert-<?php echo $this->message_stack->message('class'); ?>">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <i class="fa fa-ok-sign"></i><?php echo $message; ?>
                </div>
            <?php } ?>
            <div class="ajax-message hide">
            </div>
            <!-- End success or Error Message Display -->

            <div class="row">
                <section class="panel panel-default col-md-12">
                    <header class="panel-heading font-bold">Add Pixabay Image</header>
                    <div class="panel-body">
                        <form name="categoty_form" id="categoty_form" class="form-horizontal" data-validate="parsley" action="<?php echo base_url(); ?>pixabay/add_post" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-id-1">Pixabay Image Hash ID</label>
                                <div class="col-sm-4">
                                    <input type="text" readonly="readonly" name="image_hash_id" class="form-control" id="image_hash_id" value="<?php echo $this->input->get('pictureId'); ?>">
                                </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-id-1">Pixabay Image ID</label>
                                <div class="col-sm-4">
                                    <input type="text" name="image_id" class="form-control" id="image_id" value="">
                                </div>
                                <div class="col-sm-2">
                                    <button id="get_pixabay_image" type="button" class="btn btn-info">Get Image</button>
                                </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>

                            <?php $required = $record['id']==0?'data-required="true"':''; ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="image_name">Name</label>
                                <div class="col-sm-4">
                                    <input type="text" name="image_name" class="form-control" id="image_name" value="<?php echo $record['image_name']; ?>" data-required="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-4">
                                    <input id="image_width" type="hidden" name="image_width" value="">
                                    <input id="image_height" type="hidden" name="image_height" value="">
                                    <input id="image_path" type="hidden" name="image_path" value="">
                                    <?php
                                    $deafultImage = $this->assets->url('photo.jpg','admin');
                                    if(!empty($record) && $record['thumb_path'] != "")
                                    {
                                        $deafultImage = base_url().$record['thumb_path'];
                                    }
                                    ?>
                                    <img id="ImagePreview" width="100" class="preview" src="<?php echo $deafultImage; ?>" onerror="this.src='<?=$this->assets->url('photo.jpg','admin');?>'">
                                </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-id-1">Description</label>
                                <div class="col-sm-4">
                                    <textarea id="description" type="textarea" name="description" class="form-control" data-required="true"><?php echo $record['description']; ?></textarea>
                                </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-id-1">Location</label>
                                <div class="col-sm-4">
                                    <input type="text" name="location" class="form-control" id="location" value="Somewhere on the Earth" data-required="true">
                                </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-id-1">Photographer</label>
                                <div class="col-sm-4">
                                    <input type="text" name="photographer_name" class="form-control" id="photographer_name" value="<?php echo $record['photographer_name']; ?>" data-required="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-4">
                                    <input id="photographer_photo" type="hidden" name="photographer_photo" value="">
                                    <?php
                                    $deafultImage = $this->assets->url('photo.jpg','admin');
                                    if(!empty($record) && $record['photographer_photo'] != "")
                                    {
                                        $deafultImage = base_url().$record['photographer_photo'];
                                    }
                                    ?>
                                    <img id="ImagePhotoPreview" width="100" class="preview" src="<?php echo $deafultImage; ?>" onerror="this.src='<?=$this->assets->url('photo.jpg','admin');?>'">
                                </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-id-1">Photo sorce link</label>
                                <div class="col-sm-4">
                                    <input type="text" name="photo_source_link" class="form-control" id="photo_source_link" value="<?php echo $record['photo_source_link']; ?>" data-required="true">
                                </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-id-1">Category</label>
                                <div class="col-sm-4">
                                    <select style="height:300px;" class="form-control m-b parsley-validated" name="category_id[]" multiple="" data-required="true">
                                        <?php foreach($categories as $category):
                                            $selected = '';
                                            if(!empty($record['category_id']))
                                            {
                                                $categories = explode(',', $record['category_id']);
                                                if(in_array($category['id'],$categories))
                                                {
                                                    $selected = 'selected="selected"';
                                                }
                                            }
                                            ?>
                                            <option <?php echo $selected; ?> value="<?php echo $category['id']; ?>"><?php echo $category['category']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-id-1">Copyright</label>
                                <div class="col-sm-4">
                                    <select id="copyright_id" class="form-control m-b parsley-validated" name="copyright_id" data-required="true">
                                        <option value="">Please choose</option>
                                        <?php foreach($copyrights as $copyright):
                                            $selected = $copyright['id']==$record['copyright_id']?"selected=selected":"";
                                            ?>
                                            <option <?php echo $selected; ?> value="<?php echo $copyright['id']; ?>"><?php echo $copyright['copyright_title']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-id-1">Tags</label>
                                <div class="col-sm-4">
                                    <input id="tags" type="textbox" class="form-control" name="tags" value="<?php echo $record['tag_name']; ?>" data-required="true">
                                </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>

                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button type="submit" class="btn btn-info">Save</button>
                                    <button type="button" class="btn btn-danger" onclick="window.location='<?php echo base_url(); ?>pictures'">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </section>
    </section>
<?php $this->load->view("admin/footer"); ?>
<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class User extends REST_Controller
{
	
	function __construct() {
		parent::__construct();
		$this -> load -> library('comman_lib');
		$this -> load -> library("encrypt");
		//load all models
		$this -> load -> model("admin_model", "admin", true);
		$this -> load -> model("users_model", "user", true);
		
		$headers = $this->input->request_headers();
		$this->comman_lib->valid_hash($headers["Hashkey"]); //HashKey Checking
	}
	public function __destruct() {
    $this->db->close();
}
	function register_post()
	{
		$data = $this->get_data();
		$get_user = $this->user->checkForEmail($data['mail_id']);
		if(!$get_user)
		{
			$id = $this->user->insert($data);
			if($id)
			{
				$user = $this->user->get_by_id($id);
				$response['Message'] ="Success";
				$response['MessageInfo'] ="New User added successfully.";
				$this->response($response);
			}
			else 
			{
				$response['Message'] ="Error";
				$response['MessageInfo'] = "Internal Error Occured.";
				$this->response($response);	
			}
		}else{
			$data['id'] = $get_user['id'];
			if($this->user->update($data))
			{
				$response['Message'] = "Success";
				$response['MessageInfo'] = "User update successfully";
				$this->response($response, 200);
			}else{
				$response['Message'] ="Error";
				$response['MessageInfo'] = "Internal Error Occured.";
				$this->response($response);	
			}
		}
	}

	private function ResizeImage($sourcePath, $size, $destPath)
	{
		$config = array();
		$config['image_library'] = 'gd2';
		$config['source_image'] = $sourcePath;
		$config['new_image'] = $destPath;
		$config['create_thumb'] = False;
		$config['maintain_ratio'] = TRUE;
		$config['width'] = $size;
		$config['height'] = $size;
		$this->load->library('image_lib', $config);
		if(!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors(); exit;
		}
	}
}
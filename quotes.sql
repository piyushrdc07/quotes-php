-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 30, 2016 at 08:23 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quotes`
--
CREATE DATABASE IF NOT EXISTS `quotes` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `quotes`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `AdminID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `PerPageRecord` int(11) NOT NULL DEFAULT '10',
  `Information` varchar(255) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `UpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IsDelete` enum('0','1') NOT NULL DEFAULT '0',
  `LastLoginAt` datetime NOT NULL,
  PRIMARY KEY (`AdminID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`AdminID`, `Name`, `Username`, `Email`, `Password`, `PerPageRecord`, `Information`, `CreatedDate`, `UpdatedDate`, `IsDelete`, `LastLoginAt`) VALUES
(1, 'Super Admin', 'super_admin', 'admin@tag.com', 'f9527d25bf8a6da52ec9cf0355ae18ad3bee444f', 20, '', '2015-01-09 09:44:36', '2016-07-30 08:08:39', '0', '2016-07-30 13:38:39');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_categories`
--

CREATE TABLE IF NOT EXISTS `tbl_categories` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `status` enum('y','n') NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `tbl_categories`
--

INSERT INTO `tbl_categories` (`id`, `category`, `photo`, `status`) VALUES
(14, 'Girly', 'b2211537415dfd72b7c4c098423963f5.jpg', 'y'),
(15, 'Cats', '0a45802ec885d5f02bd7a00994965830.jpg', 'y'),
(16, 'Animals', '728e3960ab80fd2093d1328745dd987a.jpg', 'y'),
(17, 'Dogs', '0293b78f4fe1f59cd3b512f06a16357b.jpg', 'y'),
(18, 'Autumn', '09b2958bd2a8d0cdc5ff9a228fc23cf1.jpg', 'y'),
(19, 'Flowers', '75415341bf10e587955f69ce5fa9f36c.jpg', 'y'),
(20, 'Birds', '4a30e20198baaac701ed1eaa64ba8ca6.jpg', 'y'),
(21, 'Bokeh', 'fff0538935927278651c3e3a8a1c1a30.jpg', 'y'),
(22, 'Cars & Bikes', '2bdc9bd089372337473e05205f6c934d.jpg', 'y'),
(23, 'Colors', 'e55c03ca02c219fefa8a643c7d0e75c4.jpg', 'y'),
(24, 'City & Buildings', 'f0477ce0ebd7a9983bb69efe933af23b.jpg', 'y'),
(25, 'Love', '98fa93968b86276fb2ce4b8e7d5a3c11.jpg', 'y'),
(26, 'Mountains', '48033f482c9ff68e154594a8e789f50f.jpg', 'y'),
(27, 'Music', 'e40fe029ff55586943d9caa4811ecba1.jpg', 'y'),
(28, 'Nature', 'ceac390a5cb16edcde8270d95c84ad8e.jpg', 'y'),
(29, 'Patterns', 'ab12f39e641ade1eac146709ce8246bb.jpg', 'y'),
(30, 'Pink', 'db9937fe8ea68988072722c1719b48ff.jpg', 'y'),
(31, 'Monochrome', '6349971e612404ad27c99a1a9793323d.jpg', 'y'),
(32, 'Rain', '452a4c78358483d8aaaeae96a7630b61.jpg', 'y'),
(33, 'Red', '9a41123d169c7d8ae1b6b7b81d38de31.jpg', 'y'),
(34, 'Sea', '45f7d4bba1ca128f2d02e49c63fefabc.jpg', 'n'),
(35, 'Spring', '30898bc205ef5943fdc379d294c55635.jpg', 'y'),
(36, 'Summer', 'cd67b536b9e5871ef6740c507b829419.jpg', 'y'),
(37, 'Sunlight', '208ec26313299b7da0a04c398094f652.jpg', 'y'),
(38, 'Travel', 'a3a61ef6cd400a4f8977cd602b310e3c.jpeg', 'y'),
(39, 'Trees', '6c628161578a4cf6ef0635259f39e394.jpg', 'y'),
(40, 'Fantasy', 'e140235e8313d0c96a176efca4270925.jpg', 'y'),
(41, 'Festivals', '127463093fef048021ced4533c35108d.jpg', 'y'),
(42, 'Interior', '0ba789a3bbab55076e9c38652c1dab0a.jpg', 'y'),
(43, 'Fashion', '8a47eb6657c8bc69a81b05d3f26d10d7.jpeg', 'y'),
(44, 'Babies', '0233f7126fe03cfcf6d180cfad39264f.jpg', 'y'),
(45, 'Blue', '186a4ec21edb899b8fee321c68862118.jpg', 'y'),
(46, 'Wedding', 'ae020a848c1e96fa1c003ea05fd27851.jpg', 'y'),
(47, 'Food & Drinks', '8e2fa567de1c21a2a748181e33fea2dd.jpeg', 'y'),
(48, 'Quotes', '09626709de32197131da7dd8cf4caffb.jpg', 'y'),
(49, 'Landscape Mode', 'e559c33674ae9cbc646cbd3206be4744.jpg', 'y'),
(50, 'Classy', '925b5b213e2da716386fe393bd72d47a.jpg', 'y'),
(51, 'Portrait Mode', 'd30fb86f7470839cf7d61cee3cbaf05a.jpg', 'y'),
(52, 'Vintage', '2bdeac27575812e7af32348815e6ecea.jpg', 'y'),
(53, 'Ocean', 'a3ecbc323bce5b5e233116c6c8a8fd1c.jpg', 'y'),
(54, 'Ship', 'ffe8354952026bed232121fbbeb89a0e.jpg', 'y'),
(55, 'Winter', '03e10631624f1fb1019732134918a082.jpg', 'y'),
(56, 'Abstract', '4c55c6c50f868e83ef46b5cbbc70d9da.jpg', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `mail_id` varchar(100) NOT NULL,
  `device_id` text NOT NULL,
  `gcm_device_id` text NOT NULL,
  `os` varchar(20) NOT NULL,
  `manufacture` varchar(100) NOT NULL,
  `country` varchar(25) NOT NULL,
  `languagev` varchar(50) NOT NULL,
  `installed_date` date NOT NULL,
  `last_updated_date` date NOT NULL,
  `version` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=111 ;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `mail_id`, `device_id`, `gcm_device_id`, `os`, `manufacture`, `country`, `languagev`, `installed_date`, `last_updated_date`, `version`) VALUES
(1, 'mamtora.sachin@gmail.com', '', 'ey1DaqRZdTs:APA91bHb4B8Ifl07Ky87PNCYFgEXoayVdfMNvCIM_gyc1aLLHCdi2wIYSCHinz2kkJP1-ZQ2H3wlu2h926tVRIYbMpz9iq9qSnKjHyBP_4ztJxZkMOszSpBsjTnu9OXmAevYqirDrtbF', 'android : 5.1.1 : LO', 'Lenovo', 'unknown', 'en_US', '2016-03-26', '2016-03-26', 10),
(2, 'khushbooparmar265@gmail.com', '', 'dw-ZSKpcL4U:APA91bEx_5Z780m_34qatZ2Htx8i90NAi3wQn6aXiyhrSmz3SP8bajLX-jWCKr15OMPAa-SSRwVtAS-JPuAScJdQykI9hPimdp7w_393rcJAJek-bskJBRiIIdDd7aN74c2rJpwf4Jb-', 'android : 5.1.1 : LO', 'Lenovo', 'unknown', 'en_US', '2016-03-28', '2016-03-28', 10),
(3, 'piyushrdc07@gmail.com', '', 'du6sQa5V4Nk:APA91bHPb8LrkKDGlmEhqSZb7WKFizid-9jc-SQnhQQ3IUggtXqZY8lsdrviWem8NZlBSwER9vb2B0mUcb81KolW4EvR4-7cFxzarJsaaEbv2jQk8OCBURZYzEyMpimeldl5ckOGZLLH', 'android : 5.0.2 : L ', 'motorola', 'unknown', 'en_GB', '2016-02-04', '2016-02-04', 10),
(4, 'kakkad.naimish.h@gmail.com', '', 'esGO0Fje0zk:APA91bGTIqA5rELCe0250Lwaxf0_RcNPowBFR4-bLxWyxr8_ea5sCIM77_1b21ZjJsOS2CMM7859iE-bBYMf62fJymhHugP4pM4UEbZqeAtBHxYrxd6inyVEiwa3sgLJbPH_51ykZW1h', 'android : 5.0.2 : L ', 'OnePlus', 'unknown', 'en_US', '2016-03-13', '2016-03-13', 10),
(5, 'unknown', '', 'dfNZkkyKuYA:APA91bGSzG-wlrC9QGMmW6TxYzwqCf1wCxpvtKjtgWJVyAEeokSL0oWGA856DrEkYV_GUtTHMVmK3bt8EQ4dM91Lwb7uMuHEE2DPAdGfMuyrPuNS5qYpm_29sjlHqabCUH_yuk3MOwnH', 'android : 6.0 : M : ', 'motorola', 'unknown', 'en_IN', '2016-02-09', '2016-02-09', 10),
(6, 'gauravmamtora@gmail.com', '', 'dQBerMJnw7E:APA91bHm78EuBCi5JcQsIUfnLbt4kSpfs8OJEB5JPM0ruR2oliUUOTAIbKmc-GzKMExRafREdMmhiBItLXn-gbG7COZOP3eRsrSrm-UGLH9VpDDQbl-hHbwLxx7IMcNhmDDj6AXqpHEA', 'android : 5.1 : LOLL', 'motorola', 'unknown', 'en_GB', '2016-03-15', '2016-03-15', 10),
(7, 'au0654@gmail.com', '', 'fkcd0-z4qp4:APA91bH-murSDhMfLfueSpMsTNNw_NbWPw1T3pvvw7vL9WtQ-LK336tdY73LKc3IsMDxhW_r0K-lRrRrtv7Vf61-L19sX7Q-JG2IxAII5kiia4IQtryWoot1827syMJzcjf7tTFNM40O', 'android : 5.0 : L : ', 'LENOVO', 'unknown', 'en_US', '2016-02-07', '2016-02-07', 10),
(8, 'mitesh_bhadauriya@yahoo.com', '', 'dBOkvhpvk28:APA91bFFnEG-xW1p0KPQIfanKwhRvWoqfVlm_XUF2KbF1ra3Js3dFFX-__MPb_ktv_Vv1UGKNnbhDR0sLOHMaKVxyEm2kOKgyBmc_k6nD5R4jPlDMuF-_YCePBiM8ch1Fdu5fI--6iwL', 'android : 4.4.2 : KI', 'Intex', 'unknown', 'en_US', '2016-02-12', '2016-02-12', 10),
(9, 'sachin@varologic.com', '', 'cLv1t6l3l7I:APA91bHxVrSHe7Bop_tMwWBMxtI1KfxkdG73ALj5AQJOvjnOGENM1nKDlL1J5VkHbWSJYU6Qp-B4DJex018lavenBUVWtWJxPCizWGGgeZ7f7W_xjA9k3gLqt2SAc5Rrg87t7o6mbQb6', 'android : 5.0.2 : L ', 'motorola', 'unknown', 'en_GB', '2016-02-08', '2016-02-08', 10),
(10, 'mehul.bhundiya1991@gmail.com', '', 'fyy0-4ZdkwE:APA91bEA_kth_oH2ClkW6x1O62DUlbLr2J2urJooHyXhVE6Ar1WNxQ2fR1AaPScl5NlC9-8B46tZst1E-9rM78YU1NZL2mV3hAgPzSBo0Gxxf3e-T-Rp3tlxlEtuvztuEoXN9mxmcv-W', 'android : 4.4.2 : KI', 'Micromax', 'unknown', 'en_IN', '2016-02-09', '2016-02-09', 10),
(11, 'funforparthpatel@gmail.com', '', 'cHdoE_-EH_Q:APA91bE-akopHLMjaE8HtVKbE36NKC08SG9zhPoKLwze63tOadK61-nlYzhue3EE5-XlraxvYCOEPfYEb1zcSt94oSV_hWkjSy48814SdPFQOCJ-_nvTMeYn4TkmF7lq99bsSR7n6F_Q', 'android : 4.4.4 : KI', 'Xiaomi', 'unknown', 'en_US', '2016-02-09', '2016-02-09', 10),
(12, 'mail.pmitra@gmail.com', '', 'fD31PJy0gs8:APA91bFZOg0GnIfoUSAP03SyQyKcmjfcl44uqdvkkZ-Fm50LDkHnF0exL-jHq4XwERYSH6uudElQa2gO--NjOqJnK5ybFoL7q0SGKwrfpnfQTo-UCRb_AWAiSoZlaVXnu9g0MlLy65hv', 'android : 4.4.4 : KI', 'Xiaomi', 'unknown', 'en_GB', '2016-02-09', '2016-02-09', 10),
(13, 'kevinshahcool@gmail.com', '', 'eWLf4wpV_NY:APA91bEsDRaMFyzyiSiIZarWdaaNLdN8vU4Qn4L2iOegqccfxnIeKJKO9fJF6h2wbqf-dMYKU-97bfaiP_45SD_ph3d1fPJZQRVhu5i66Gsr3BgC1bR8N27Hd2Ys2kLCIaA1AL3k5o6z', 'android : 4.4.4 : KI', 'Xiaomi', 'unknown', 'en_US', '2016-02-12', '2016-02-12', 10),
(14, 'mtzsadri@gmail.com', '', 'cbRJgEQrebg:APA91bHLnzzwo6WZL9W634QmTKnDkuqZEDsK4yi9pr1CbL7I8-2DFDrjjuBx1olZiESue7B3MB-pPbBPa3r0nTNrKOtrjlVJPohoz4spnHXD3k7iCfMCfhqz72d4Rf7a0SajwKKC9pb4', 'android : 4.1.2 : JE', 'Sony Ericsson', 'unknown', 'en_IN', '2016-02-09', '2016-02-09', 10),
(15, 'sunilkarulkar@gmail.com', '', 'cdEsfxjKzWs:APA91bEeG6muxRd8aa6J71S3xL3YnA8ZSrtu_dpVvnNtdiB1dnGoVP1G2ShNUwE8iLLNZiB9bz1m9VHKCIMFz-X9dSOO5TxIODzNFbx9SEi-mg_jl-3qzVBF4Oc1NZu6JXbIvqdHx1q6', 'android : 5.0 : L : ', 'Micromax', 'unknown', 'en_US', '2016-02-09', '2016-02-09', 10),
(16, 'njvado@gmail.com', '', 'fCB0gwDwDZ0:APA91bFbCN9do8P1G6Dk131EllpUFvSEBajC-wjD28aSPsPR_HvzbKPwSUDkt-oSjTm0IM3hj0uceMV3vbbqgz02iBYbUuHH4aAYTVZAwQYsI_HrTmWUh0gL508BmDEk5HJDj7N9xlho', 'android : 5.1.1 : LO', 'samsung', 'unknown', 'en_US', '2016-02-09', '2016-02-09', 10),
(17, 'nakadividya@gmail.com', '', 'eeBahiDnmS8:APA91bGNPWkGxjJRgjfy0qY3LhJ2WFA-f1UDpOd8OK0Dtx6I20QPt0jMZGAgH1BWP3abSwfZX0IvmFIE1yXjXXTlzLdV55dDGOMYjMYIZAtlGa_3RxWiA_mHA0vTAIw5DtBTeBTm7xYU', 'android : 4.4.2 : KI', 'samsung', 'unknown', 'en_US', '2016-02-29', '2016-02-29', 10),
(18, 'rajyog.shan@gmail.com', '', 'fbmJrMjyUoo:APA91bFClQ59VeW5krp5Wm4VB87uy2GKpNaIZeyxyBuDtm5epPkdnY5ebxjxmibC12MwAbVVOCLLfvxL5k42gSfDw7lBTJ-68B6UUx1LZgEZuvsQDCItWQwdMe1tZ8HTCRKGH2xaWZ33', 'android : 4.2.1 : JE', 'XOLO', 'unknown', 'en_US', '2016-02-17', '2016-02-17', 10),
(19, 'raju.parmer@yahoo.com', '', 'dH-4f8Zhr5k:APA91bHqEh7pMQGrR7OlKQpkUZdPMuFP-D1pLSVvd9Zdmcnm73xrRrdO-n_iHV6RdjUf2nlbnYeQBHgBSyviwlAdRDNJOt3pyfn47hKERfLFBG_VnijZDsXN2xL8H5bU_zQALhS1DrkG', 'android : 5.1.1 : LO', 'samsung', 'unknown', 'en_GB', '2016-02-09', '2016-02-09', 10),
(20, 'anuj.shah95@gmail.com', '', 'eYbdwlMXXPk:APA91bHr5ghSQtRVvuGox6zFBlNyWS7qDjHWLUuw83o8ZeY9OaeKrAzsvROde1dQaEteLUbwiiSt8P8IxrZ_QJ_HJIb8CyNJghfEyGPiLAn0xTNsghpmB6SnbZNW6trbli1qFrCn0WxU', 'android : 4.4.2 : KI', 'LENOVO', 'unknown', 'en_US', '2016-02-09', '2016-02-09', 10),
(21, 'vipulnewaskar7@gmail.com', '', 'fXteb1rmvew:APA91bE4KqATXtyx7onaG01-1D5IcNoKgE_lg4hv2i9e6GBU99dvZNxUtJFTXiqNUzzjcst--KiIRJSFHw_mMpgYEESPB6zLHJrZPMHU3UZr69KLsaAWoyw_e2QfPCcY3KveHXF-Zmbn', 'android : 4.1.2 : JE', 'samsung', 'unknown', 'en_GB', '2016-02-09', '2016-02-09', 10),
(22, 'pragnesh_pml9@yahoo.com', '', 'cA7tvtYRtUE:APA91bGssjxbUnr7eAJV9XhU8alCc-RLqx_0czqmYl2bLFLWCSfn_lVbvXvrAJDGNzTRjYzWALjYjCqCbDJWcsFzCiXSzktdADu8wmt_VbSi9Zw9vgY--Qsp0XVvB1V0bEFiCgufudjB', 'android : 5.0.2 : L ', 'motorola', 'unknown', 'en_IN', '2016-02-09', '2016-02-09', 10),
(23, 'arjundhameliya99@gmail.com', '', 'drL_Z3Tz8f4:APA91bHu41ljg1x4o7NwzjiE26Z6W6bsTLRqZ9Tt10t-4a_yOxBJjt8xuV_Qi77b6lIV9MLKE4DPZdjQaRI1KhFPHrA1DBxhnAmMhTvQoK9W8vbT0Iq8YjCTux58u1HzpgqSaJfW7VoQ', 'android : 4.2.2 : JE', 'Lenovo', 'unknown', 'en_IN', '2016-02-12', '2016-02-12', 10),
(24, 'bhardwaj.bhardwaj.patel@gmail.com', '', 'cvRndNUpdzc:APA91bFP5sgB62Te3HAyvYWUq5Z4d_Lh21evSjZaInZfzjSvcklKrO-UM_LlstAGlClG9Iune8GsKZWMKKKtpwPEqIrb2-8_n3nH--kD9tsJ1KwcLPeP9jQ2NbFkUP5JHRmCCl85NNif', 'android : 5.0.2 : L ', 'Xiaomi', 'unknown', 'en_GB', '2016-02-16', '2016-02-16', 10),
(25, 'dharmeshdhameliya99@gmail.com', '', 'efSskjM2y1I:APA91bHjgcyaXFbFRJsIyXBIbiPrGBh2_Hb5bocITID6TQJn-X0bj0gRtEdh3kohx4zT1MIV5QiTtUsnJMUoVg3mdH-wuD3pcRnsEi06u2CGOdEiiIsu1b_Hv_-j_JNjUawX5spCcCxR', 'android : 5.1.1 : LO', 'samsung', 'unknown', 'en_GB', '2016-02-09', '2016-02-09', 10),
(26, 'dilip.vora07@gmail.com', '', 'e1YPJWv5LO0:APA91bEStgWKUbUaVgGPfSjx4TAeGpsHfz0r5n2nK3_aOw3R1lBQY6vihmtmgYH7I9dBXbqEkd9RoNKbta4ZHaLw3rwkDJj2S44Ya7OFHVOGCUxGytt2WXgMnuAaZPkQutjUa6i-91lx', 'android : 5.1.1 : LO', 'OnePlus', 'unknown', 'en_US', '2016-03-24', '2016-03-24', 10),
(27, 'nirali.sathwara93@gmail.com', '', 'ePPvECS9_oM:APA91bFWFpzLR77kIv1v_p0f0BCeldSrCZOv1Tb-0rVpFsPAPN0sDzLKixii_gMC0vV2aq5HVlBowUbGwaRaZjAi8GXe8ALkaeEJLY33y387Rk1qYIRnBhJIgLXQ8DZszjY4ZDmMxrOq', 'android : 4.4.4 : KI', 'Xiaomi', 'unknown', 'en_US', '2016-02-10', '2016-02-10', 10),
(28, 'swap_vyas83@yahoo.co.in', '', 'ccctPzTBsM8:APA91bE7YiufXgwt5OT2D8r28eV1ss2m_zZ-Wl9znFHZN-Sfp1rshFb7FGBie_wghQVmZnBXbokPgVx9yWvm-zNomZt2FHZvZFHxlXc-fZFLFL0SUbW12nNRQr96zET2CmPZEQf1gIej', 'android : 5.0.2 : L ', 'Xiaomi', 'unknown', 'en_US', '2016-02-10', '2016-02-10', 10),
(29, 'mehulrughani@gmail.com', '', 'fCAqWf9JX5g:APA91bH4Ytqje5i6Fw3kuHH4tTI1ZPBs3yN7MSlTrdhktFYp2LJhQ5ON9yMOIcstgslE1F140z-yoqiKbnZo_gQyt01EYHMQTyGAa65re0AYP-c_MuLUOW4C8yuPhtMXH2NoxVezHAi4', 'android : 5.1.1 : LO', 'YU', 'unknown', 'en_US', '2016-02-17', '2016-02-17', 10),
(30, 'harit.mehta1@gmail.com', '', 'dVY81YlLl64:APA91bGAVv2ni5WwUmItjyKc6YAYPBUmQF_lEBn-qrP__FQl7S4q9wCAlztOSSBwy6wwr_tlSsgfz1z6W_xK9wkAQuhBgl1M3gHIture5oHfnSAge22n4hnZE8EpkTQc4I-NpVgNHEjP', 'android : 4.4.4 : KI', 'samsung', 'unknown', 'en_US', '2016-02-09', '2016-02-09', 10),
(31, 'talsaniyahitesh@gmail.com', '', 'ek_dUAF56cc:APA91bG67Uz6QjK_-KJBNQfD2ixmc1voK4ePRcQtj5QRfkbo6qor8CWnm8zMcsVlQG2uiioBXhhIR6rXxh16VQwPqbtH6m4KRHoVIPmzQl2Y3ft-3s9lfsVPnQtmeRKPOdkQj3Hk-Wpc', 'android : 5.1.1 : LO', 'Sony', 'unknown', 'en_IN', '2016-02-10', '2016-02-10', 10),
(32, 'prithviraj238@gmail.com', '', 'cEuvGXJb4nY:APA91bHsC0tl1OMX4QToMkd1OtLLx-qcwqndZSmEV4OaqTsmdjSMeyi6ejSjR1qwc-kannm1QinVw0rFj8Lla3W9_TAMQpHxk1PBTYEJXyzttFmzFH1KLnTsmcbYbF0I8pPJAVvQaZCL', 'android : 5.1 : LOLL', 'motorola', 'unknown', 'en_GB', '2016-02-10', '2016-02-10', 10),
(33, 'jayraj9495@gmail.com', '', 'dDT9Dt8zhOU:APA91bG7_fulUyn9xiBm0gYmE2bEWOVf1H6DXSXIQybxN-qszch-ECg81IijgCVbEsQG4BKJwDxIgti7ovYhlRA46n2L5KXasRLG35rzbzKjOE6fdZ-SfkHJsPemX3RBeF0vaCSMSRRS', 'android : 4.4.2 : KI', 'Micromax', 'unknown', 'en_US', '2016-02-11', '2016-02-11', 10),
(34, 'kushhirani7945@gmail.com', '', 'dl9O3U65pu8:APA91bGdePkh_d5iypFdJ4IfeQvPatBjsLriZwe7BBWIV6btcnfPr0dT5HroEDHdrZQa-DAHZiMAus2CNnsjk6arGKk4mPQRUamUGO6q42tGcpYKSihgpjAp6gBYzwyzxNUJ7PVHfFpb', 'android : 4.2.2 : JE', 'Lenovo', 'unknown', 'en_IN', '2016-02-11', '2016-02-11', 10),
(35, 'mihirjogi@gmail.com', '', 'dxvoNWrnpz4:APA91bHnfv6i6sLLtPs9jv2-UzKZ5Pj0lMhTY3hkqHHzKQnom59IRIRZGBjo8m-f1gWCDWJ-1GZDRAKyGa5OaUAYFg-IWRXZOBRLlC88kBeuvRNnkkk0n7sCOkh4OTR4MeIiYTAU0wGB', 'android : 4.2.1 : JE', 'XOLO', 'unknown', 'en_US', '2016-02-11', '2016-02-11', 10),
(36, 'aalialbarbary@gmail.com', '', 'c2QPreVh0ao:APA91bHeLvcqZIFz2LXqSb4IvcU7ngkzQKAxHpup7SKOXk0GUBQe114IQIZqeD9n7KZY8g27sSg-2b3ZFWhPytx3sVRfQOJcwRawTwsHzgPu1xBddjSjmzBF_jArJcN3nInV7XQtEaTQ', 'android : 4.4.2 : KI', 'HTC', 'unknown', 'en_US', '2016-02-13', '2016-02-13', 10),
(37, 'aanayakhan66@gmail.com', '', 'dKmMhHWr93Y:APA91bF6b2Rg9R832ux_9DSRAP7poxE4SWlQT5jnF0vtFmHj0Hwsa7IIirelyMvkj8wDYWpI6sbhmMwTTqT8EDCxxy18NHtDX0v18JXCwDeJsDN09xRadXV10ydqKz2bJGm8athXSHuK', 'android : 5.1.1 : LO', 'samsung', 'unknown', 'en_GB', '2016-02-13', '2016-02-13', 10),
(38, 'arvind94275@gmail.com', '', 'cGx11E3N5Zw:APA91bH5TnOGG50I82g8VlIJkurb8969L2113xuqoaTom0q01due2bUvCLslZEGnl9npqPFUgH782Fqh4g1aFCQ8LmqGdYoji9W2Go7o15kpX3HMy1tW03ucGgJjnm546UdIygNW42Lg', 'android : 5.1.1 : LO', 'Sony', 'unknown', 'en_US', '2016-02-13', '2016-02-13', 10),
(39, 'klea.pretty@gmail.com', '', 'craP5QrsK-c:APA91bELHhwN2QHnkPR0wTVL7hdhHZr5IltjG50WJI1_0OJJOyNph5G0kOY-rfUHvQ3swMNiDJgIQjwGTFwFnRHtxdqIi72I2nPqhbHSeuVk1ZykW_8hjKGCDlS9S4K4UOQVY3LeSjHt', 'android : 4.4.2 : KI', 'samsung', 'unknown', 'en_GB', '2016-02-13', '2016-02-13', 10),
(40, 'madi.nagla@gmail.com', '', 'cSHlPYZY4MI:APA91bGBvvXMhmCb_Ply5IrSGsdbOymdQzy_Xg8p2Q5yiRZ33GYtuOtwbRPn4OOZqfWmwUbAu92YTaP8RBhjbRnMVk-DXzO2oNXoEHLfXu9nYvLtc2S7W8wCii-tw6EohV6zQlCXcjuK', 'android : 4.1.2 : JE', 'HUAWEI', 'unknown', 'en_US', '2016-02-27', '2016-02-27', 10),
(41, 'simha.sp@gmail.com', '', 'e6VE6bfDeho:APA91bEFw4Gnazlurc6KcHOEgUTbzix-5bBujTU2W7BFs2ICjbZ3IaUqc-XxgWkrKf2jbvcxaQoBTCvrvLtY7ciFASGYShxZbBqA2Q3Ot2aEbWMCpEhxDeuM2W-9tBvmsG4v8iY669lH', 'android : 5.1.1 : LO', 'YU', 'unknown', 'en_US', '2016-02-14', '2016-02-14', 10),
(42, 'marwaeltahiali@gmail.com', '', 'cVFeqRocVOE:APA91bFjoqcX9wR9gT_bYPm9FrNHyOE_8xZaz-p13X9sE8y8YHEQI0D_bkxqw_T7WclFs6quyQfXjaAwlDJ2Hv-YmI4Ai3DvaBzyx8_MndQFM_W8oQ4G5G6om-XQnEUB8uhtwAP01oZe', 'android : 4.3 : JELL', 'samsung', 'unknown', 'en_US', '2016-02-14', '2016-02-14', 10),
(43, 'vivekkothari910@gmail.com', '', 'fqofL4d8_YI:APA91bFw54Zmgl9ErQko6omKjExHtN8H1Ple_w7bOiBt2FfrxcIrhXHz8MYeJtdhXO43cnjg6qeRW1TOKF5266UoKufX3SZ277WXKAIBT0Xk8OLA7N_uku3QUhTFs6PFjvYstirV3ixU', 'android : 5.1 : LOLL', 'HTC', 'unknown', 'en_IN', '2016-02-29', '2016-02-29', 10),
(44, 'wanessa.bastos@gmail.com', '', 'cy5TumQF6tw:APA91bE3jurzMP1xFTOQeYzw-rw8FyqKhYl5MUlInflcvmLr_85BwrunDjUxMCPeewJmaWDaWEucKelSkqhXURx-t8BLOX_CndJ8Zw5rO4BSyDtpxCzjcLmb72UU2ehGokK1vBBXioRy', 'android : 5.1.1 : LO', 'samsung', 'unknown', 'pt_BR', '2016-03-05', '2016-03-05', 10),
(45, 'www.63mz46@gmail.com', '', 'fzPHDgizj0M:APA91bGCj5H_YB1HJTDeCuxIq5GKzOetVFlohw5fvsKaGBQ0Lbx1XGdJPo-pLpWH7QhqNn1gHfalKAfxAaGsxxqG052dpIio7j-K6WyU2U7DXeSovPl4VClUjEG2UvoqdmCaoCvLAi_Y', 'android : 4.1.2 : JE', 'samsung', 'unknown', 'fa_FA', '2016-02-24', '2016-02-24', 10),
(46, 'varologictest@gmail.com', '', 'cLv1t6l3l7I:APA91bHxVrSHe7Bop_tMwWBMxtI1KfxkdG73ALj5AQJOvjnOGENM1nKDlL1J5VkHbWSJYU6Qp-B4DJex018lavenBUVWtWJxPCizWGGgeZ7f7W_xjA9k3gLqt2SAc5Rrg87t7o6mbQb6', 'android : 6.0 : M : ', 'motorola', 'unknown', 'en_GB', '2016-02-16', '2016-02-16', 10),
(47, 'mayurbhola@gmail.com', '', 'eRTs9P68Oz8:APA91bHvpPWHLGPDEnL61PyrF4iNiew5QfUa_KGDhakbAfZL0W01UNP8l7wOeMYRoqZeoRa9EzcfgL5kz5NH3VgSpBzUOi4RZfGGVsOcSjqAfiEZh3Wh5GoPy-XztXVJcxAUCmu9I0pH', 'android : 5.1.1 : LO', 'LGE', 'unknown', 'en_IN', '2016-02-16', '2016-02-16', 10),
(48, 'abdalwhabm7@gmail.com', '', 'cM4EtuBpmNI:APA91bE7YhO_AgqjHcRMU4aKGj1e1yqPyQ3oCbx6HTIYOB7mvCk5rtspznZ7wjYpvlgdP0300kJv73nzMEFJDIlkB35So8nK6uQg8xbJoB6a5NFRDVl0y2kOwa0Mu3vV5_PChgEc-XB0', 'android : 4.1.2 : JE', 'HTC', 'unknown', 'ar_AE', '0000-00-00', '0000-00-00', 10),
(49, 'vmenefield@gmail.com', '', 'cYqzy506L8c:APA91bGh6iuysZBNqgAHkSORD3J-C1D6_a5ndWtS5A6pkB9cPz1l5qpc5L7P-smL4Yn4qUy8gJYeaxwNfVLI_d3Oqs8qoZmg1oPjbX-6fver3NDN0foZ2FNHSbJpNTtKiEPM4O6rnMux', 'android : 4.2.2 : JE', 'TCT', 'unknown', 'en_US', '2016-02-17', '2016-02-17', 10),
(50, 'tejalporiya@gmail.com', '', 'eOF4ftAMjLk:APA91bGTSKHMP1bA4VgDo_VU19yeOVK6c1wtnwqNTwORmq_d3vkdeq5MJV6rGrQWAcpBTWYz5d49ja7t7WCYV5-UXw83lk7LGKZVf8XS3JJZI5-OC64_eLqtAyTANTaanVADbW8jMWFP', 'android : 5.1.1 : LO', 'Sony', 'unknown', 'en_US', '2016-02-17', '2016-02-17', 10),
(51, 'usmannoor20@gmail.com', '', 'cQfcvJTHYl4:APA91bEzPsKBchKGQS-oNAREnLOb7rUnRce9UM1VNKfXxu-Rc-FyU7VriUiKrWyl9keUcht2pb2ckiZA-OIUtwDH3JclXJwMMac-el2NDehWdykcOWtv418tnoZ176fYP7pt3pc2FmC5', 'android : 4.4.4 : KI', 'samsung', 'unknown', 'en_GB', '2016-02-18', '2016-02-18', 10),
(52, '', '', 'esfJNREqP6o:APA91bFUiVG4h_Kh2fUsyUVK2la40e4QTiARmDRgn27FmX6WV6EjQ5LaOCMdC6Cx_0Rw3ECE4K3S1jI2WvC0T6M44aVIAisTOa5vnmeGVu_8233Firjs4kpsiCFAzEAN84c1VU9ev1Nj', 'android : 6.0.1 : M ', 'Huawei', 'unknown', 'en_US', '2016-03-25', '2016-03-25', 10),
(53, 'rysio.wachu@gmail.com', '', 'fPYBnQ9yDT8:APA91bH5X6SnB_LT2lCGPbJIS59PjX-w_e87_QL9TXt_H0t3ifOUeILE1GkdfAxUyoHPuFq8yqgtcePam7G3RS0ogo7joa0Z8YsBZWH13Bjf4nSHR7prsO0QpnuxoEHnfQYOsjlaPp0q', 'android : 4.1.2 : JE', 'samsung', 'unknown', 'pl_PL', '2016-02-19', '2016-02-19', 10),
(54, 'andresilva112000@gmail.com', '', 'dYnVs9Z4qUc:APA91bFpaRt4XrSOLZwdie48cH5Dbsf31lZkgMmpTndP6FOk7Es3Ya6QcXH3ADUFdjC8YjkPJk937Mja5s4WnG2gYYPcxqeiBw-AQxlzPZC_LW-LAfh4olMTLy-O06OQiyWUewJZMhYJ', 'android : 4.4.2 : KI', 'alps', 'unknown', 'pt_PT', '2016-02-19', '2016-02-19', 10),
(55, 'harmeetchavda16@gmail.com', '', 'fF9lOoNM53E:APA91bEQAzJ4nJzEjvR44ga_HsEHVBudRvffUk1Yuscsp5g3UQ2jTX0KyHrhH43hd6umt3ts36ZUuYpXxm5qEi-X_3V2Pc0WgbK34PYMsrHI9JYj2PwZSWgr8CO8oBB0vuAnjpG-UW4O', 'android : 4.4.2 : KI', 'HTC', 'unknown', 'en_IN', '2016-02-20', '2016-02-20', 10),
(56, 'karotaradharmesh@gmail.com', '', 'cfY06Eepm5Y:APA91bG8gtK85U-Gr0ybA8U8wqypafbye8aMRLZA-DS3KnAke1QOgXiwTx-XIQkNU35utxzFIH1y-iKoSlfq9P1wlaPWLaiaKc7uARKULO-At3lVXiG18HfHrS5VZlr71QxOdyIJKyhm', 'android : 4.2.2 : JE', 'OPPO', 'unknown', 'en_US', '2016-02-20', '2016-02-20', 10),
(57, 'qmodem100@gmail.com', '', 'ds_blrIf_F0:APA91bEOE_CCJCgY3Mo0A9hJnAje84Nk-2OKSyWnIpVLeeE7dN2E2p-6mlJii-P28x-_42jkFxGd9Bo7vOgcBeqU-By4Irm7MlR-ECfv3gpM01dYD5Zs95-OnjcG9BuwI51inmapnZZg', 'android : 5.0.1 : L ', 'Tianshi Digi-Land', 'unknown', 'en_US', '2016-02-20', '2016-02-20', 10),
(58, 'mohamardi86@gmail.com', '', 'eSRlgxucZ_Q:APA91bEBXovmR7IAheh_ehEgNIYle8aKMVCA5D4hSUvPrCFH00t9SggTlTAqAIJN0ef3c4W7VGhPrNrN2BCsKtQUMPxNBWUkoMplc7KsB0pbPoYavEjTYJDk38y9X7O60jYNfMdVLftT', 'android : 4.4.2 : KI', 'samsung', 'unknown', 'fa_IR', '0000-00-00', '0000-00-00', 10),
(59, 'slahalbohnfr@gmail.com', '', 'da8f-u2dd3w:APA91bHk6ZOiqalFsWHSOPL2PaHd1Byb2Yi8g6VsXySBXkZJ10Homdhk_ShdwadC03HqWGJ3hPM9zYRYAl08mpTZ8iGBzqd6JXRBN6W2GuRqJ5Uq7nT26eTfGz8a8wwPXzYei9U0jV34', 'android : 4.4.2 : KI', 'HUAWEI', 'unknown', 'fa_IR', '0000-00-00', '0000-00-00', 10),
(60, 'chiranjeev.patadiya007@gmail.com', '', 'eMcddwwn5Go:APA91bGMfhkuro_v0uNmIQTtwyWci_DbCdnk5j6D9b4Q988gXdmtxaZPmphFyhhyXeYo366X8JvxJgNTTPHgv9t0HkO1awSq5JPmjDAcOV2pV2UJkukkjITrjxSJkOHZWH18RX7Zrbky', 'android : 4.2.2 : JE', 'Sony', 'unknown', 'en_US', '2016-02-24', '2016-02-24', 10),
(61, 'cdeloor20@gmail.com', '', 'f-1n4UqfyqM:APA91bGJER2ws1nYcPZf5mCkD5DCPfFddHD0TuRFy258jSejPzDwkghivCdV6XBueFb1e56kP6KOyLv01kvr5NQa81Ub0rTULdBpkIShFL27DKsyimqbJ9AR84CBo928G4W8QOIBZ0_z', 'android : 4.4.4 : KI', 'Sony', 'unknown', 'fr_BE', '2016-02-24', '2016-02-24', 10),
(62, 'eldajangulli55@gmail.com', '', 'e1xGdOmFQJU:APA91bHszUr5-1yejEJ_748DEECg-x7pEldiaxRA9ndL4gWS0PuqcxNPx-2SL3XEFaeA0XF8DALcJ8YCZiSC4k_QvmVv9bgUTXYUn3Bs3rvVvloYVOaaa2UqN01JRwXU3TLD4KE9TkrC', 'android : 4.4.2 : KI', 'samsung', 'unknown', 'en_GB', '2016-02-26', '2016-02-26', 10),
(63, 'hadravovamarie@gmail.com', '', 'ftX4JlPAesk:APA91bHkRLKQlyhmW5PwCTUh-x3I64v9XhWcSZHMwZvia-CqBG907iiC3qdWTs0r4hA5QnaUg5AP1aRZwVnP8z7gTep0asmsP0oRNbDRvN7R07_7QVrBtonK7VRaEmBaO8kmbbZzrKGz', 'android : 4.2.2 : JE', 'samsung', 'unknown', 'cs_CZ', '2016-02-26', '2016-02-26', 10),
(64, 'heidihappy52@gmail.com', '', 'cyrY2xdL3u4:APA91bFTtR4f-Jz4J7JNvKeISsTmeB9OwTZ5-hoPyekPNYOwGH3jPt4nMW8M4n994L3GKESjRtLszxqu07JoKu0JbBwXZJ_7L6CZQ8z1M82Hnbu9Cf540KAArgbwZonSxC-Nalctzv0G', 'android : 4.4 : KITK', 'LGE', 'unknown', 'en_US', '2016-03-02', '2016-03-02', 10),
(65, 'salahalbokhanfar@gmail.com', '', 'fW16u7Ci6bM:APA91bFdBQSyrA8nPZDZ3ANWbSv0dXKvaPYcbfBK52esTxfsftn9TVln6rw-Xw_fhWCWdUoT3s9OjrstQZdBsZ2Lf4xgC8V2Jgk4qLKriZHko5cjpqL6LAcsktngIUH8ZwnGAc31f-Lk', 'android : 4.4.2 : KI', 'HUAWEI', 'unknown', 'fa_IR', '0000-00-00', '0000-00-00', 10),
(66, 'alexthegreatgreat5@gmail.com', '', 'dLJTi7LRERk:APA91bGD6t7uGyhEY7qfLDKEM3JDWQDp6CkWLERdPRKTEkqxFu1FF5qxi3tWEFjjyU_KZCGHvR5ykZzLChmcbxbo09g3MyXuN_eomCo9lOnjBBPnEEhgNufchu4gS3aNvPawBeWzdc_R', 'android : 5.1.1 : LO', 'samsung', 'unknown', 'el_GR', '2016-03-04', '2016-03-04', 10),
(67, 'bi4re68@gmail.com', '', 'dTskRGBmLCM:APA91bFIbY0r9KaGbLX2IUqdwQL5wnBclh7scNMOyJc5BF-vAeVzhRfFNWcRRPdO6GtRwpI9Z4sRj-Y2TtlKFfwt74PoX-F7JwDX950tuvolGKaN1fkdMeTCUAy_BzqTVXOlnF-qXEmo', 'android : 5.0.2 : L ', 'HTC', 'unknown', 'en_US', '2016-03-16', '2016-03-16', 10),
(68, 'mizan2642@gmail.com', '', 'dSJZuhSHSVA:APA91bFZSt3Hayeq4vhnFJkU9CZqBdyKq7q1ymHPLGX80IBPejRC2gcDDi_KEdXLlkMzIx6NgslAMkwYbvLUIhQnRPu8CQQUFw3OjBaf-HKRTHsrVAKLjgdON0fkLaD5Ryc-sNbdCzqj', 'android : 4.4.2 : KI', 'HUAWEI', 'unknown', 'en_GB', '2016-03-06', '2016-03-06', 10),
(69, 'kyle4033@gmail.com', '', 'd8hpQ1z9cMQ:APA91bFtRLJyLhYq2cRaJtY4t00qJS8Vlbsi5MySc4POFUhfsd4Sw4dPiq2fJTteyImr8t4E7FoWrX1rPENOVVNOSklf1wSMlopvMWESqeYjdlyFIMZfvybFcw7Z8r7BU-NIutfmRGS3', 'android : 5.1 : LOLL', 'HTC', 'unknown', 'en_US', '2016-03-06', '2016-03-06', 10),
(70, 'nat8914@gmail.com', '', 'eWbseydsfqE:APA91bHsHyGC0JzuxnRalyawK-K6fQz1qcJIbGs6azISbELi7FjZZRvrYrS1A5XY-ekw8CxI9-RVwG7dZEe3f9wOE6LSKKezgAMN3agsxCpBK_JVM8s5QxEaTaEl7f4ABvrSE5mqc9Jd', 'android : 5.1.1 : LO', 'samsung', 'unknown', 'en_CA', '2016-03-06', '2016-03-06', 10),
(71, 'ben.marsat@gmail.com', '', 'cFXGW1SpiaM:APA91bEYWXEa3SMa5Jw_0tzuTElhLbPZArrBmJMYSVpeVML-MjF6xX9nr-g6VWBkd0IudCJIScNTDjxPcjz8lMboLWBC3EMW6Pkvw0kJkHcV2fa-yQPsOCZEqnE1OV7hcjgvXPcesldn', 'android : 5.1 : LOLL', 'motorola', 'unknown', 'en_GB', '2016-03-07', '2016-03-07', 10),
(72, 'dharmishthamamtora@gmail.com', '', 'cAmsuy-gbYQ:APA91bFsBvLHNKdBn9rpdHCA3rGes4RbjsrDFss-Y0pZ3XivQSOA7fK8nkUowLZT_N2zYrrn2mvlZ4gVIZ8gEqrhG-t2J7_Qip1dKrgwGzGQ90bdJDhzws-yMT4yPx5eFMzyDrV0HOBd', 'android : 5.0.2 : L ', 'motorola', 'unknown', 'en_GB', '2016-03-06', '2016-03-06', 10),
(73, 'phillipskjj@yahoo.com', '', 'dtvLnN74IxM:APA91bHDmpZdWOvSN0fC-P-iGSpRfAKmTNHTmgdVKVPrlbDkzZ8eTozwAo27gN7NDg1BdelO9T3kgdS9lE9zfGytWZiqHylCBdaKl9YY6-nTXfFTgIk3wzM_Dde3d12gIZf-AuAMhGTh', 'android : 5.1.1 : LO', 'samsung', 'unknown', 'en_US', '2016-03-06', '2016-03-06', 10),
(74, 'slolix1962@gmail.com', '', 'eDkMkguZZuc:APA91bFbWorjatg6gissKTMHKyhCP-HChonBQSFZ4VAc6rr2ndRE2VFbUv7oJgxpUzV5BzfuJrcLGc3R4opY9WdN_or-oxXrS6-xexG8IVqp7F99byV-pCRiwcOX2BbjxoB5z2CGCo0a', 'android : 5.1.1 : LO', 'samsung', 'unknown', 'ru_RU', '2016-03-07', '2016-03-07', 10),
(75, 'robdavison@outlook.com', '', 'eChCHX-ZdYg:APA91bF_eKOSDKHfR2mReXFzHpjYf27A4DjrgCUoKe96A_QTf8bDiREcwoHZzhbV46EqSKxqqU3sqmuyS0wI7oQWlZT6nuF8e5XfUpugNCZmZuLN3BM2sD7mhPfcgqQ_AelyERhlP4UC', 'android : 5.0.1 : L ', 'samsung', 'unknown', 'en_GB', '2016-03-10', '2016-03-10', 10),
(76, 'adelidovalle681@gmail.com', '', 'd0TzUcQdIlM:APA91bFN2D26KgR2PHQcSCMa1WCuHXzrS4rtY_fnF4uF96ytyDfadrAAcJDUysrIrnXFEmN94UHu7DJnGqPL9SlnMGDOqOl57E5To-h0Vzgsgm6ScdARpDfSOAFJoMyfHra53eK-tv-1', 'android : 4.2.2 : JE', 'MS5', 'unknown', 'pt_BR', '2016-03-10', '2016-03-10', 10),
(77, 'hkwebtek@gmail.com', '', 'fVa9c2KSTKg:APA91bHlXv8cnZxpBRctT6JRUxTD5hvn5FoGFD61jXpx3MNs-qrq6V1oJ1SMXZOgqDeq3OfANbsK2XpYe0f5h8yk8QHVw9DiyFfo_kD_qhtc7ASF5xo5wuxcoDebxl724fBBRa7SGDd3', 'android : 5.0 : L : ', 'asus', 'unknown', 'en_IN', '2016-04-12', '2016-04-12', 10),
(78, 'www.1363zahra@gmail.com', '', 'c7mKVJxJABM:APA91bF0F2qCno4_FvBqsBLdR4w84allbbGjQjc3F8vfUP0BJuAPgIWRgCl2t_dlsBF4PSkWRMAMO2notz_GeS0_8vgStuhJUzAfg2-w6iQ1Rek9yGpxWCyPIyQNYoir74AKfmlTcrpf', 'android : 4.1.2 : JE', 'samsung', 'unknown', 'fa_FA', '2016-03-13', '2016-03-13', 10),
(79, 'jw965764@gmail.com', '', 'dkKh_ylL4QE:APA91bH-OUfawylKKoD5hYFYXAIENKxf67CSuaMXVO2fSXH1U2cdET6OIqHQDDb0xAb4MvpCUOI7b5tAhzHWXvQg4rhdygUh4kgt9vje2epUoOkfBeJgTVhc-9cS3dieGX4hZyEnjaqX', 'android : 5.0.2 : L ', 'samsung', 'unknown', 'en_US', '2016-03-12', '2016-03-12', 10),
(80, 'hmvillegas1414@gmail.com', '', 'dXggCzf12uo:APA91bFVN5QdfdqaddNBzZov56U0Oni48U7dJNCJrf5iI7OplJX5f4sOtarTUTQhDOPDm68yj7X-6DmddclEEyObCVD6AukGnTcDEAM6736_yC6F60fIdCiUBGF1iKwc5-p5EwswEbGh', 'android : 5.1 : LOLL', 'HTC', 'unknown', 'en_US', '2016-03-12', '2016-03-12', 10),
(81, 'dylan.olfers@gmail.com', '', 'dksdZR-ufxw:APA91bE-aaM1vo8U24Q-fnCIgZZOSXtZm7thM6_kqj_bkR1GqXOT1eIIDs9ijPIDeRG9OWTps4MTXIEeM7doK8kySx-F1YkjOaAKRcclLri__MH3lF_aVgU0e84RDweizv__rDr61R3t', 'android : 5.1.1 : LO', 'OnePlus', 'unknown', 'en_GB', '2016-03-12', '2016-03-12', 10),
(82, 'musilijosh846@gmail.com', '', 'frCbb6xfoto:APA91bETpMptjYTL08g609-ch4hhY0MeR3r9f2wn3kJW9oa8JpmewvdmtjEPrUIky4TyR48adamTtB3ORfwEMse_XQo4Xcw_KoKzbotTyR56cg0gZxXn1tX9lhn03Wd8WxVel89kRWPq', 'android : 4.4.2 : KI', 'TECNO', 'unknown', 'en_US', '2016-03-26', '2016-03-26', 10),
(83, 'elmagic_ali22@yahoo.com', '', 'dTIV2aYidWA:APA91bGSQFse-2NCmmbtE9fT9X4OF0BkqG4Qh1xEf8OuTjxhYigShGRPBPovp0pfwR5So3qqKTVXP4O0s2QCZmSq03oELixQcivbpXv1jRjBDpFRTTXnGpbR0XXSd5xw7g0r5BFUrjIx', 'android : 5.1 : LOLL', 'Sony', 'unknown', 'ar_EG', '0000-00-00', '0000-00-00', 10),
(84, 'knebarle@gmail.com', '', 'fGCXZsPoCzk:APA91bHYsdmR1eznVgIs7Pl4OII-cLkYbqyR41uYTQnCgzZ65rCEGQjT4FbQBCYxruMz7yMlMtHbDyUQ1VWx_OHrbIO8jL59leE7kI5m-1oUTKitGqpyFloq2HejoS_1x14SGa6nDBo6', 'android : 5.1.1 : LO', 'samsung', 'unknown', 'en_PH', '2016-03-14', '2016-03-14', 10),
(85, 'jamesjking94@gmail.com', '', 'c7yugyErPVo:APA91bGWBW3sNyViRaWV3vXd4ofJpOQL-MoJsYk9X9jX1sjwLdS9OX4DBbdWUZtQRjcNAH0K8Qr9Z8vvcburqCQ-I40JJ8KKfYN0VhIG9Rc5-jb5C1jEizhjC--f8ShBwZU5vMqL1KJ4', 'android : 5.0 : L : ', 'samsung', 'unknown', 'en_GB', '2016-03-15', '2016-03-15', 10),
(86, 'hslssldfkf@gmail.com', '', 'cADb5hz6ROk:APA91bFoDRqxTtNmDUkTXNhc_GVNwIlk7h_tMJRWgnJp38AxPYjEqbtIWE8HAi94L1Vhi0tt-lh_x8XZrFTcnN2LPnYg-oofwXejetxY3DtNLvi8bx_-moWF5rxM9ZV9vihdKh1B9voz', 'android : 4.4.2 : KI', 'intex', 'unknown', 'en_US', '2016-03-15', '2016-03-15', 10),
(87, 'varshagolaniya2015@gmail.com', '', 'dttdr5eQ6qo:APA91bGU9j_4yP5_y4UhAGbWUZV0aNrtQHdDEmyO-oy5VsW3NYomq-VElrLi925zXWk3QOnbYoLoOrQs6B-euRegyZiNSznbofXqEtzlHDxNNgnFfVfe6ANrW0CgmdpHavvyD0rljRUi', 'android : 5.1.1 : LO', 'OPPO', 'unknown', 'en_GB', '2016-03-21', '2016-03-21', 10),
(88, 'orourke.s.michael@gmail.com', '', 'fKSntb6i3OE:APA91bE7rkSADLZ1j81-5oOWIH32ISj2PTdmEUROEo7gKeoIlIu7Om2dUj0XNVfg_LZT7vZx0ZmJ0s481txwolLhqiEzxpdPasOjOzCz1CsafNNWIYviDSKNCfaG2A-Y90jxDhrB_Tyz', 'android : 5.1.1 : LO', 'samsung', 'unknown', 'en_US', '2016-03-15', '2016-03-15', 10),
(89, 'lolorached@gmail.com', '', 'f6Nr269a9YQ:APA91bFpSMGqQCOx_3o3wzM82tDwNz9LOx6tonvuTEQ6F0eEvFB-cdaE2MZISOXsG5ZRvwoOQBL8SO4Sunz8KrDX0p0K2nujtai8dilwkg3JebyC8bpYWCmDlKWSbHK8wJdGYEjGX978', 'android : 4.2.2 : JE', 'samsung', 'unknown', 'en_GB', '2016-03-17', '2016-03-17', 10),
(90, 'sunbat1980@gmail.com', '', 'fUIkuaXJxzM:APA91bGSH1lV2guEa_b44QGxYU5c68S5Qk5QEUf1s3NjPHtO3VYJDERigCH-O0j_c42hfxE0F9BF7WM3PmgS2nZuRZPemMHyu5Z4mmRyDQCdPEsyL72z7xnmx4iH-fJafAWpdHhkjD_c', 'android : 4.2.2 : JE', 'samsung', 'unknown', 'en_GB', '2016-03-17', '2016-03-17', 10),
(91, 'informramin@yahoo.co.uk', '', 'evAXre1TlnU:APA91bGwl_ObsSyk1_40GFP9MEhCUWlaCyGkR_pemiFsufAeNxkfJEFW3SwBItwfM5gfdQCrjmSdYITR-t_hT7sfSQTw2IUI8dv-OzqL7grvKmidrYjsV9p9Ql4Q_phjexvGvLWAG4G6', 'android : 5.0 : L : ', 'samsung', 'unknown', 'en_GB', '2016-03-18', '2016-03-18', 10),
(92, 'jose528@gmail.com', '', 'emFcLu0LYKg:APA91bGsKj9ze1xeqLoc9re61H0T0T5GbKaZ6hwyJ7ksr5iE7dJMQHMge5unFgzUp4IUX7DUY3tRoaYj_mrAWz8_sBgazC8YBo-hvro3vJz7lqXwPs8sJszc8TldvS4JoJsf-wGT4SO2', 'android : 5.1.1 : LO', 'OnePlus', 'unknown', 'en_GB', '2016-03-17', '2016-03-17', 10),
(93, 'haley.young@gmail.com', '', 'exIyrN8jWi8:APA91bEQHQwbtrkyE4-Sob6lZUll_qot30dK63B22CCzLe4OkbILiagJj9ecu2YatW50CTNPpliMxxg5eGQbPwbPCCgjJPh6IvjKuzSSRJA8L625WnaKLavfPvWtGKxLGZm89lT7HZbV', 'android : 5.1 : LOLL', 'HTC', 'unknown', 'en_US', '2016-03-21', '2016-03-21', 10),
(94, 'dziag.katarzyna@gmail.com', '', 'fQaz3hg3qdE:APA91bEQV0ks8_qITbBk18qeBbhPZbsc0qk0kHKjnrnuVijYvpgGlZesHji8WCaYbvLCd4g6U-n0oy0CocVakfNESdmMrvY_tVjhaD9VpZehmB-uhLI7tmcjm3MgNyUfSTmA9nPmiifF', 'android : 5.1.1 : LO', 'Sony', 'unknown', 'pl_PL', '2016-03-19', '2016-03-19', 10),
(95, 'nvsathyaprabhu@gmail.com', '', 'd5jN4DuBN-E:APA91bHeBrlJA2ygAFILMdOGST354O2LA4-yzIz2bZsNZTSuug2Ct6zEM-NJ6aADm3LnvnfJGpEpj66n3UZ1d_bQIP1ZKHCLHXBa0W8Y37pxxxhlBFd2IdupLtJQDwAK5UCUJ39cfC9k', 'android : 5.1.1 : LO', 'samsung', 'unknown', 'en_GB', '2016-03-20', '2016-03-20', 10),
(96, 'clairechew4040@gmail.com', '', 'eyBkJBPH5Xg:APA91bGQHW0iU-tt88cLXIu4eIF75-aT5M9QsTvrD_rW6RoHKcuHDPrzwUJrQM1DetDuWNU-iXBMkIobM9jlhQpeOQOyhbCQrgHZ6-smKaWdsY020JPsVfsx2uD4d5k3MbYHT-JOfgSl', 'android : 5.1.1 : LO', 'OPPO', 'unknown', 'zh_CN', '2016-03-21', '2016-03-21', 10),
(97, 'mr.norullahi@gmail.com', '', 'fyVIxBORG10:APA91bHqs82y46su1OFRLzarVkRY0EIkDvg1hAtGhj7738BSfCI-bmtXaz3J1ofFBcHikzSyuKDhZKcvXkBzRW9-IyOxsE7H9EYKS9LKb2VsOCmeoy3NehnKUXoDISPcnJH4Mzr81-yi', 'android : 5.0 : L : ', 'samsung', 'unknown', 'en_US', '2016-03-21', '2016-03-21', 10),
(98, 'victor.cuen12@gmail.com', '', 'd2GcHOwmIOA:APA91bGT7NUryiQq63OJKKDgfnr6dnvDPdExJVKDj0-gei1ZqCYxAtFI6Z_0pf70Kb7N3xwzocbPi47JwBUrkoWk48NxWsVo1CRyi4BOo3d8FDkscM24nsP7q9AkBe7ojpuUzT9O43iO', 'android : 5.1.1 : LO', 'samsung', 'unknown', 'en_US', '2016-03-21', '2016-03-21', 10),
(99, 'gotstevecsgo@gmail.com', '', 'fopQlrPnHPU:APA91bFS2sgKaIoa3HftVH9N_qrUrn0-m1oBzEB4CQoiO92GOnGtavyPT9t93UZVlKOxuhp4Neo03uHzne9eJs_X06NCXFLEovOslN5UOArdUR2BaeixTir4YCTXmUuNJc2nP121h1C1', 'android : 5.1.1 : LO', 'samsung', 'unknown', 'en_US', '2016-03-21', '2016-03-21', 10),
(100, 'nyprospect@gmail.com', '', 'dsuPqhJBINM:APA91bEpmf3c6rYpGnKSeOoVCWBl_-7vo8ff71tsSBjxW2udZDfDeZeC-8qcQbWTAWoxGRQkCy7iBOHF5FrxEGESZpG0x0NipwqicoZdkBVOvPTr8wWiSvdsVRXVHjcCNxpEFZXRm7eH', 'android : 4.4.2 : KI', 'LGE', 'unknown', 'en_US', '2016-03-22', '2016-03-22', 10),
(101, 'www.06346zahra@gmail.com', '', 'ew1OicJNYTE:APA91bGMfIU0-lcRaE8-0vvOd_XlJWV1bD4AnQYmm6gknO5y7r68AuXFpwiF8BLVaGEJJYly-r3wWkEqsxZWfEH_H8iEaYyJQr2D6Oi6DtDmLU2Rj2zvE23ys82ifa5XoQaSGwLOFl_4', 'android : 4.1.2 : JE', 'samsung', 'unknown', 'fa_FA', '2016-03-23', '2016-03-23', 10),
(102, 'salartradingroad78@gmail.com', '', 'd7KDwAyiKyY:APA91bF4_lvw8yJsDt6FGEfoAN716j8ga-_4qR67ukCf7c6uTuYUeV6ljolYLe1tBAvJ6DSdFd3dz_d_4NmTitn-42hrNi5YC60apTKk1jHyoYzml0pCmc0ozmKirlK0Eq8DzNdj62gA', 'android : 4.4.2 : KI', 'POSH', 'unknown', 'en_US', '2016-03-29', '2016-03-29', 10),
(103, 'bryan.rich3@gmail.com', '', 'fKnBoUowD7w:APA91bF7YWhElCKyqUU5IvfcgvURXxCXaEv2a7SN8gRQTQmc-i_VAETRfJb-LNJxLgFt_jZijygf8Cw0orPOuI6VF6imZURqKtZnl5mYUWSPmI4xmArF3R9pCFaZ-3QknTvNGbuAGhq6', 'android : 5.1.1 : LO', 'samsung', 'unknown', 'en_US', '2016-03-22', '2016-03-22', 10),
(104, 'wandaspino13@gmail.com', '', 'd3AHzvilCUo:APA91bHEtrPfebmyqPiv1XrQkUjrEMxrvDTBxHX7Nb1pADanwXL7r-U_ns6hKPNB7vUfXZS9cYT0UXoD_uxJfwoPZFhhpEhHfFoKYRFPgYfcn_oZBvVvDS4JT5qHgZ0lq-cbN7sQJtqq', 'android : 5.1.1 : LO', 'samsung', 'unknown', 'en_US', '2016-03-24', '2016-03-24', 10),
(105, 'kathyballew19@gmail.com', '', 'eiIoAznqmVU:APA91bFNhc47IYy6s_Jr97hWIRmUKWOTlm0nCKknuRCTQZuJ7AxIkxbDzITpn8Az5CEKNWPmmnZNgk4KAwcK1Tc659s72fqeUgtQluZsQkZhlEH94Ol_u1-lItZ6n-0-zipKjzjIO5C-', 'android : 5.1 : LOLL', 'ZTE', 'unknown', 'en_US', '2016-03-24', '2016-03-24', 10),
(106, 'nikolay.srebrev@gmail.com', '', 'c08cvW7cLHY:APA91bFzGVXNMppM1xmHiFE7v99CUYsC0SktssEQ8_UoTsPkMHysqKlr-q-5Ir4um4BytVoz7M0T7PII2MSbSVCZJO4nkh1cJa_qgZYZ-fuYU6U3LNzjSfH7GofbQpC1ZYUenaE1zwFb', 'android : 4.2.2 : JE', 'samsung', 'unknown', 'bg_BG', '2016-03-25', '2016-03-25', 10),
(107, 'fakhera.amiri@gmail.com', '', 'euJdx-tUgy4:APA91bHDUUFbwnYoU29vrnfRkMxSYNooSnjoEVHBZyG7iQ0UYzDe6Q767jmjAbmUgnMdmZbURmbpXlbfAbQIJyZw1I1JfDDS27_JsqzvZjnaIBEqx7RBSQP3hwB5CedodkS7UKq0tgaB', 'android : 5.1.1 : LO', 'samsung', 'unknown', 'en_US', '2016-03-26', '2016-03-26', 10),
(108, 'wendythackray@live.co.uk', '', 'fTi6bW82puk:APA91bHf2qpccM1P1afnUnHHQaUhUtR8p5Z3WXL20_Bcfb0jIJqbqjhE8HAx_xep48Q1Y91lDTAJr9zBsTiXh-A-5CKJvv3j1OTd0S1tH32rb37P30c40YqLa4ZXUdeYMQugYWsOCz4P', 'android : 5.0.1 : L ', 'Archos', 'unknown', 'en_GB', '2016-03-26', '2016-03-26', 10),
(109, 'a1simplythebest@gmail.com', '', 'eBLIjWYWrzI:APA91bHrZZwj-YryVo4r4yGClYjIjNgib7OfrivNtiRmm2ZtkZG1yNuRYWVmkEY3sDvRC7ZTVXY9ZS-Fn8h8zXpcJVp6scPJXIqSfGvmDhgBwLS_F8OHhQpDlZ1QPS3X_Uhmp5iNO3Eu', 'android : 4.4.2 : KI', 'LGE', 'unknown', 'en_US', '2016-03-26', '2016-03-26', 10),
(110, 'bffranzmdg014@gmail.com', '', 'chU6FGUSUaU:APA91bEiCPRDATPIBzDcEu24H19HFBJR7UL6KtEpLmlncUNUbwEMK1Jk6xojt6dOGfNGm-OOeqVynRd7tF0zAgIVd7NXButlnNf9Xo23mGKLhtoR9AjcXZxNzrsN15mAS8Nei1bfr1Sz', 'android : 5.0 : L : ', 'asus', 'unknown', 'en_US', '2016-03-27', '2016-03-27', 10);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
